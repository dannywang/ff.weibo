﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Web.Script.Serialization;

namespace Mongo
{
    public static partial class DBTools
    {
        #region 用户相关

        /// <summary>
        /// 注册新用户
        /// </summary>
        /// <param name="newUser"></param>
        /// <returns></returns>
        public static User SignupNewUser(User newUser)
        {
            SafeModeResult re = MongoDBHelper.MongoDBHelper.InsertOne<User>(Collection.User, newUser);
            return GetUserInfo(newUser.LoginName);
        }

        /// <summary>
        /// 用户登录认证
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static User UserLogin(string loginName, string password)
        {
            User result = new User();
            //var query = new QueryDocument { { "LoginName", "1055550@176.com" } };
            //where LoginName='' and password=''
            QueryDocument query = new QueryDocument();
            query.Add("LoginName", loginName);
            query.Add("Password", password);
            result = MongoDBHelper.MongoDBHelper.GetOne<User>(Collection.User, query);
            return result;
        }

        /// <summary>
        /// 更新用户信息
        /// </summary>
        /// <param name="newUser"></param>
        public static void UpdateUserInfo(User newUser)
        {
            User updateUser = new User();
            QueryDocument query = new QueryDocument();
            query.Add("LoginName", newUser.LoginName);

            updateUser = MongoDBHelper.MongoDBHelper.GetOne<User>(Collection.User, query);

            if (updateUser != null)
            {
                updateUser.NickName = newUser.NickName;
                updateUser.Description = newUser.Description;
                MongoDBHelper.MongoDBHelper.UpdateOne<User>(Collection.User, updateUser);
            }
        }

        /// <summary>
        /// 根据用户登录名获取用户
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public static User GetUserInfo(string loginName)
        {
            User result = new User();
            //var query = new QueryDocument { { "LoginName", "1055550@176.com" } };
            //where LoginName='' and password=''
            QueryDocument query = new QueryDocument();
            query.Add("LoginName", loginName);
            result = MongoDBHelper.MongoDBHelper.GetOne<User>(Collection.User, query);
            return result;
        }

        /// <summary>
        /// 修改用户头像
        /// </summary>
        /// <param name="newUser"></param>
        public static void UpdateUserFace(User newUser)
        {
            User updateUser = new User();
            QueryDocument query = new QueryDocument();
            query.Add("LoginName", newUser.LoginName);

            updateUser = MongoDBHelper.MongoDBHelper.GetOne<User>(Collection.User, query);

            if (updateUser != null)
            {
                updateUser.Face = newUser.Face;
                MongoDBHelper.MongoDBHelper.UpdateOne<User>(Collection.User, updateUser);
            }
        }

        /// <summary>
        /// 根据用户名获取用户信息
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public static User GetUserNewInfo(string loginName)
        {
            User result = new User();
            QueryDocument query = new QueryDocument();
            query.Add("LoginName", loginName);
            result = MongoDBHelper.MongoDBHelper.GetOne<User>(Collection.User, query);
            return result;
        }

        /// <summary>
        /// 根据id获取用户信息
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static User GetUserInfoById(string uid)
        {
            User result = new User();
            result = MongoDBHelper.MongoDBHelper.GetOne<User>(Collection.User, uid);
            return result;
        }

        /// <summary>
        /// 搜索好友
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static List<User> SearchUserByKey(string key)
        {
            List<User> result = new List<User>();
            List<User> all = new List<User>();
            //QueryDocument query = new QueryDocument();

            //query.Add("NickName", key);
            all = MongoDBHelper.MongoDBHelper.GetAll<User>(Collection.User);

            var users = from it in all
                        where it.NickName.Contains(key)
                        select it;

            foreach (var item in users)
            {
                result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// 添加好友
        /// </summary>
        /// <param name="relation"></param>
        public static void AddFriend(Relation relation)
        {
            SafeModeResult re = MongoDBHelper.MongoDBHelper.InsertOne<Relation>(Collection.Relation, relation);
        }

        /// <summary>
        /// 获取我的好友列表
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static List<User> GetUserFriendListByUId(string uid)
        {
            List<User> result = new List<User>();
            List<Relation> relationList = new List<Relation>();
            QueryDocument query = new QueryDocument();
            query.Add("UserId", uid);
            relationList = MongoDBHelper.MongoDBHelper.GetAll<Relation>(Collection.Relation, 100, query);
            for (int i = 0; i < relationList.Count; i++)
            {
                result.Add(relationList[i].RelationUser);
            }
            return result;
        }

        /// <summary>
        /// 移除好友关系
        /// </summary>
        /// <param name="FriendUserId"></param>
        public static void RemoveFriend(string FriendUserId)
        {
            List<Relation> result = new List<Relation>();
            QueryDocument query = new QueryDocument();
            query.Add("RelationUserId", FriendUserId);
            result = MongoDBHelper.MongoDBHelper.GetAll<Relation>(Collection.Relation, 100, query);

            MongoDBHelper.MongoDBHelper.Delete(Collection.Relation, result[0]._id.ToString());

        }

        /// <summary>
        /// 获取所有用户
        /// </summary>
        /// <returns></returns>
        public static List<User> GetAllUser()
        {
            List<User> result = new List<User>();
            result = MongoDBHelper.MongoDBHelper.GetAll<User>(Collection.User);
            return result;
        }

        /// <summary>
        /// 更新用户状态
        /// </summary>
        /// <param name="newUser"></param>
        public static void UpdateUserStatus(User newUser)
        {
            User updateUser = new User();
            QueryDocument query = new QueryDocument();
            query.Add("LoginName", newUser.LoginName);

            updateUser = MongoDBHelper.MongoDBHelper.GetOne<User>(Collection.User, query);

            if (updateUser != null)
            {
                updateUser.Status = newUser.Status;
                MongoDBHelper.MongoDBHelper.UpdateOne<User>(Collection.User, updateUser);
            }
        }

        /// <summary>
        /// 更新密码
        /// </summary>
        /// <param name="newUser"></param>
        public static void UpdateUserPassword(User newUser)
        {
            User updateUser = new User();
            QueryDocument query = new QueryDocument();
            query.Add("LoginName", newUser.LoginName);

            updateUser = MongoDBHelper.MongoDBHelper.GetOne<User>(Collection.User, query);

            if (updateUser != null)
            {
                updateUser.Password = newUser.Password;
                MongoDBHelper.MongoDBHelper.UpdateOne<User>(Collection.User, updateUser);
            }
        }

        #endregion

        #region 微博相关

        /// <summary>
        /// 发表新微薄
        /// </summary>
        /// <param name="wb"></param>
        public static void PublshNewWeiBo(WeiBo wb)
        {
            SafeModeResult re = MongoDBHelper.MongoDBHelper.InsertOne<WeiBo>(Collection.Publish, wb);
        }

        /// <summary>
        /// 根据作者获取已发布微博
        /// </summary>
        /// <param name="WriterId"></param>
        /// <returns></returns>
        public static List<WeiBo> GetPublishedWeiBo(string WriterId)
        {
            List<WeiBo> result = new List<WeiBo>();
            QueryDocument query = new QueryDocument();
            query.Add("WriterId", WriterId);
            result = MongoDBHelper.MongoDBHelper.GetAll<WeiBo>(Collection.Publish, 100, query);
            return result;

        }

        /// <summary>
        /// 根据主题获取微博
        /// </summary>
        /// <param name="WriterId"></param>
        /// <param name="themeId"></param>
        /// <returns></returns>
        public static List<WeiBo> GetPublishedWeiBoByTheme(string WriterId, int themeId)
        {
            List<WeiBo> result = new List<WeiBo>();
            QueryDocument query = new QueryDocument();
            query.Add("WriterId", WriterId);
            query.Add("Theme", themeId.ToString());
            result = MongoDBHelper.MongoDBHelper.GetAll<WeiBo>(Collection.Publish, 100, query);
            return result;
        }

        /// <summary>
        /// 获取已发布微博
        /// </summary>
        /// <param name="weiboId"></param>
        /// <returns></returns>
        public static WeiBo GetPublishedWeiBoById(string weiboId)
        {
            WeiBo result = new WeiBo();
            result = MongoDBHelper.MongoDBHelper.GetOne<WeiBo>(Collection.Publish, weiboId);
            return result;
        }

        /// <summary>
        /// 删除一条微博
        /// </summary>
        /// <param name="id"></param>
        public static void DeleteWeiboById(string id)
        {
            MongoDBHelper.MongoDBHelper.Delete(Collection.Publish, id);
        }

        /// <summary>
        /// 发表评论
        /// </summary>
        /// <param name="com"></param>
        public static void PublshComment(Comment com)
        {
            SafeModeResult re = MongoDBHelper.MongoDBHelper.InsertOne<Comment>(Collection.Comment, com);
        }

        /// <summary>
        /// 获取已发布评论信息
        /// </summary>
        /// <param name="weiboId"></param>
        /// <returns></returns>
        public static List<Comment> GetPublishedCommentByWeiboId(string weiboId)
        {
            List<Comment> result = new List<Comment>();
            QueryDocument query = new QueryDocument();
            query.Add("CommentWeiboId", weiboId);
            result = MongoDBHelper.MongoDBHelper.GetAll<Comment>(Collection.Comment, 100, query);
            return result;
        }

        /// <summary>
        /// 消息盒子获取数据（私信，评论）
        /// </summary>
        /// <param name="uid"></param>
        public static string GetNewMessageByUserId(string uid, string loginName)
        {
            
            //查詢当前用户信息
            User currentUser = DBTools.GetUserInfoById(uid);
            //查询评论信息
            List<Comment> list_Comment = MongoDBHelper.MongoDBHelper.GetAll<Comment>(Collection.Comment);
            //查询所有未读私信
            QueryDocument query_msg = new QueryDocument();
            query_msg.Add("ReceiveUserLoginName", loginName);
            query_msg.Add("Status", 0);
            List<ShortMessage> list_ShortMessage = MongoDBHelper.MongoDBHelper.GetAll<ShortMessage>(Collection.ShortMessage, 100, query_msg);
            List<Comment> list_Comment_Result = new List<Comment>();
            for (int i = 0; i < list_Comment.Count; i++)
            {
                if (list_Comment[i].CommentWeibo != null)
                {
                    if (list_Comment[i].CommentWeibo.Owner.LoginName == loginName && list_Comment[i].Status == 0)
                    {
                        list_Comment_Result.Add(list_Comment[i]);
                    }
                }

                if (DBTools.GetCommentByTargetId(list_Comment[i].TargetId) != null)
                {
                    string tLoginName = DBTools.GetCommentByTargetId(list_Comment[i].TargetId).PublishUser.LoginName;
                    if (tLoginName == loginName && list_Comment[i].Status == 0)
                    {
                        list_Comment_Result.Add(list_Comment[i]);
                    }
                }

            }
            var data = new { list_Comment = list_Comment_Result, list_ShortMessage = list_ShortMessage, currentUserId = currentUser._id.ToString() };
            string result = DBTools.JsonSerialize(data);
            return result;
        }

        /// <summary>
        /// 获取所有收到的评论
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public static List<Comment> GetMyReceivedComment(string loginName)
        {
            //查询评论信息
            List<Comment> list_Comment = MongoDBHelper.MongoDBHelper.GetAll<Comment>(Collection.Comment);
            //查询所有未读私信
            QueryDocument query_msg = new QueryDocument();
            query_msg.Add("ReceiveUserLoginName", loginName);

            List<ShortMessage> list_ShortMessage = MongoDBHelper.MongoDBHelper.GetAll<ShortMessage>(Collection.ShortMessage, 100, query_msg);
            List<Comment> list_Comment_Result = new List<Comment>();
            for (int i = 0; i < list_Comment.Count; i++)
            {
                if (list_Comment[i].CommentWeibo != null)
                {
                    if (list_Comment[i].CommentWeibo.Owner.LoginName == loginName)//|| list_Comment[i].ToCommentLoginName == loginName)
                    {
                        list_Comment_Result.Add(list_Comment[i]);
                    }
                }
                if (DBTools.GetCommentByTargetId(list_Comment[i].TargetId) != null)
                {
                    string tLoginName = DBTools.GetCommentByTargetId(list_Comment[i].TargetId).PublishUser.LoginName;
                    if (tLoginName == loginName)
                    {
                        list_Comment_Result.Add(list_Comment[i]);
                    }
                }

            }
            List<Comment> returnCommentList = new List<Comment>();
            returnCommentList = list_Comment_Result.OrderByDescending(p => p.AddDate).ToList();
            return returnCommentList;
        }

        public static Comment GetCommentByTargetId(string targetId)
        {
            Comment result = new Comment();
            //QueryDocument query = new QueryDocument();
            //query.Add("TargetId", targetId);
            result = MongoDBHelper.MongoDBHelper.GetOne<Comment>(Collection.Comment, targetId);
            return result;
        }
        #endregion

        #region Message
        /// <summary>
        /// 发送私信
        /// </summary>
        /// <param name="message"></param>
        public static void SendMessage(ShortMessage message)
        {
            SafeModeResult re = MongoDBHelper.MongoDBHelper.InsertOne<ShortMessage>(Collection.ShortMessage, message);
        }

        /// <summary>
        /// 获取当前用户的私信
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public static List<ShortMessage> GetShortMessageByLoginName(string loginName)
        {
            List<ShortMessage> result = new List<ShortMessage>();
            QueryDocument query = new QueryDocument();
            query.Add("ReceiveUserLoginName", loginName);
            result = MongoDBHelper.MongoDBHelper.GetAll<ShortMessage>(Collection.ShortMessage, 100, query);
            return result;
        }

        /// <summary>
        /// 更新所有已读私信
        /// </summary>
        /// <param name="uid"></param>
        public static void UpdateShortMessageStatus(string uid)
        {
            //查詢当前用户信息
            User currentUser = DBTools.GetUserInfoById(uid);

            //查询所有未读私信
            QueryDocument query_msg = new QueryDocument();
            query_msg.Add("ReceiveUserLoginName", currentUser.LoginName);
            query_msg.Add("Status", 0);
            List<ShortMessage> list_ShortMessage = MongoDBHelper.MongoDBHelper.GetAll<ShortMessage>(Collection.ShortMessage, 100, query_msg);
            for (int i = 0; i < list_ShortMessage.Count; i++)
            {
                list_ShortMessage[i].Status = 1;
                MongoDBHelper.MongoDBHelper.UpdateOne<ShortMessage>(Collection.ShortMessage, list_ShortMessage[i]);
            }

        }

        /// <summary>
        /// 更新所有已读评论
        /// </summary>
        /// <param name="uid"></param>
        public static void UpdateCommentStatus(string uid)
        {
            //查詢当前用户信息
            User currentUser = DBTools.GetUserInfoById(uid);
            //查询评论信息
            List<Comment> list_Comment = MongoDBHelper.MongoDBHelper.GetAll<Comment>(Collection.Comment);
            List<Comment> list_Comment_Result = new List<Comment>();
            for (int i = 0; i < list_Comment.Count; i++)
            {
                if (list_Comment[i].CommentWeibo != null)
                {
                    if (list_Comment[i].CommentWeibo.Owner.LoginName == currentUser.LoginName)
                    {
                        list_Comment_Result.Add(list_Comment[i]);
                    }
                }
                if (DBTools.GetCommentByTargetId(list_Comment[i].TargetId) != null)
                {
                    string tLoginName = DBTools.GetCommentByTargetId(list_Comment[i].TargetId).PublishUser.LoginName;
                    if (tLoginName == currentUser.LoginName)
                    {
                        list_Comment_Result.Add(list_Comment[i]);
                    }
                }

            }
            for (int i = 0; i < list_Comment_Result.Count; i++)
            {
                list_Comment_Result[i].Status = 1;
                MongoDBHelper.MongoDBHelper.UpdateOne<Comment>(Collection.Comment, list_Comment_Result[i]);
            }
        }

        /// <summary>
        ///回复评论 
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="cid"></param>
        /// <param name="msg"></param>
        public static void ReComment(string uid, string cid, string msg)
        {
            //Comment ReCom = new Comment();
            //ReCom.AddDate = DateTime.Now;
            //ReCom.CommentMessage = msg;
            //ReCom.CommentWeibo=DBTools.GetPublishedWeiBoById(
            //SafeModeResult re = MongoDBHelper.MongoDBHelper.InsertOne<Comment>(Collection.Comment, com);

        }
        #endregion

        #region 投票
        /// <summary>
        /// 发起新投票
        /// </summary>
        /// <param name="t"></param>
        public static void CreateTicket(Ticket t)
        {
            SafeModeResult re = MongoDBHelper.MongoDBHelper.InsertOne<Ticket>(Collection.Ticket, t);
        }

        /// <summary>
        /// 根据tid获取投票信息
        /// </summary>
        /// <param name="tid"></param>
        /// <returns></returns>
        public static Ticket GetTicketByTid(string tid)
        {
            Ticket result = new Ticket();
            QueryDocument query = new QueryDocument();
            query.Add("tid", tid);
            result = MongoDBHelper.MongoDBHelper.GetOne<Ticket>(Collection.Ticket, query);
            return result;
        }

        /// <summary>
        /// 投票
        /// </summary>
        /// <param name="t"></param>
        public static void AddUserTicket(ChooseTicket c)
        {
            SafeModeResult re = MongoDBHelper.MongoDBHelper.InsertOne<ChooseTicket>(Collection.ChooseTicket, c);
        }

        /// <summary>
        /// 获取投票结果
        /// </summary>
        /// <param name="tid"></param>
        /// <returns></returns>
        public static List<ChooseTicket> GetTicketResultByTid(string tid)
        {
            QueryDocument query = new QueryDocument();
            query.Add("TargetTid", tid);
            List<ChooseTicket> listAll = new List<ChooseTicket>();
            listAll = MongoDBHelper.MongoDBHelper.GetAll<ChooseTicket>(Collection.ChooseTicket, 1000, query);

            return listAll;
        }


        public static bool CheckTicket(string tid, string login)
        {
            QueryDocument query = new QueryDocument();
            query.Add("TargetTid", tid);
            query.Add("ChooseUserLoginName", login);
            ChooseTicket cc = MongoDBHelper.MongoDBHelper.GetOne<ChooseTicket>(Collection.ChooseTicket, query);
            if (cc == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region 其他工具
        /// <summary>    
        /// 将对象转换为 JSON 字符串。    
        /// </summary>    
        /// <param name="obj">要序列化的对象。</param>    
        /// <returns>序列化的 JSON 字符串。</returns>    
        public static string JsonSerialize(object obj)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            return jsSerializer.Serialize(obj);
        }

        #region 图片缩放，多种指定方式生成图片
        /// <summary>
        /// 图片缩放
        /// </summary>
        /// <param name="originalImagePath">原始图片路径，如：c:\\images\\1.gif</param>
        /// <param name="thumbnailPath">生成缩略图图片路径，如：c:\\images\\2.gif</param>
        /// <param name="width">宽</param>
        /// <param name="height">高</param>
        /// <param name="mode">EQU：指定最大高宽等比例缩放；HW：//指定高宽缩放（可能变形）；W:指定宽，高按比例；H:指定高，宽按比例；Cut：指定高宽裁减（不变形）</param>
        public static void MakeThumbnail(string originalImagePath, string thumbnailPath, int width, int height, string mode)
        {
            System.Drawing.Image originalImage = System.Drawing.Image.FromFile(originalImagePath);

            int towidth = width;
            int toheight = height;

            int x = 0;
            int y = 0;
            int ow = originalImage.Width;
            int oh = originalImage.Height;

            if (mode == "EQU")//指定最大高宽，等比例缩放
            {
                //if(height/oh>width/ow),如果高比例多，按照宽来缩放；如果宽的比例多，按照高来缩放
                if (height * ow > width * oh)
                {
                    mode = "W";
                }
                else
                {
                    mode = "H";
                }
            }
            switch (mode)
            {
                case "HW"://指定高宽缩放（可能变形）                   
                    break;
                case "W"://指定宽，高按比例                       
                    toheight = originalImage.Height * width / originalImage.Width;
                    break;
                case "H"://指定高，宽按比例   
                    towidth = originalImage.Width * height / originalImage.Height;
                    break;
                case "Cut"://指定高宽裁减（不变形）                   
                    if ((double)originalImage.Width / (double)originalImage.Height > (double)towidth / (double)toheight)
                    {
                        oh = originalImage.Height;
                        ow = originalImage.Height * towidth / toheight;
                        y = 0;
                        x = (originalImage.Width - ow) / 2;
                    }
                    else
                    {
                        ow = originalImage.Width;
                        oh = originalImage.Width * height / towidth;
                        x = 0;
                        y = (originalImage.Height - oh) / 2;
                    }
                    break;
                default:
                    break;
            }

            //新建一个bmp图片   
            System.Drawing.Image bitmap = new System.Drawing.Bitmap(towidth, toheight);

            //新建一个画板   
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);

            //设置高质量插值法   
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

            //设置高质量,低速度呈现平滑程度   
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            //清空画布并以透明背景色填充   
            g.Clear(System.Drawing.Color.Transparent);

            //在指定位置并且按指定大小绘制原图片的指定部分   
            g.DrawImage(originalImage, new System.Drawing.Rectangle(0, 0, towidth, toheight),
                new System.Drawing.Rectangle(x, y, ow, oh),
                System.Drawing.GraphicsUnit.Pixel);
            try
            {
                //以jpg格式保存缩略图   
                bitmap.Save(thumbnailPath, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch (System.Exception e)
            {
                throw e;
            }
            finally
            {
                originalImage.Dispose();
                bitmap.Dispose();
                g.Dispose();
            }
        }
        #endregion


        /// <summary>  
        /// 将指定的 JSON 字符串转换为 T 类型的对象。  
        /// </summary>  
        /// <typeparam name="T">所生成对象的类型。</typeparam>  
        /// <param name="input">要进行反序列化的 JSON 字符串。</param>  
        /// <param name="def">反序列化失败时返回的默认值。</param>  
        /// <returns>反序列化的对象。</returns>  
        public static T JosnDeserialize<T>(string input, T def)
        {
            if (string.IsNullOrEmpty(input))
                return def;
            try
            {
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                return jsSerializer.Deserialize<T>(input);
            }
            catch (InvalidOperationException)
            {
                return def;
            }
        }


        public static bool IsEmail(string str_Email)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(str_Email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }
        #endregion

    }
}
