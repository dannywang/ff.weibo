﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mongo
{
    public static class Collection
    {
        /// <summary>
        /// 用户信息
        /// </summary>
        public static string User = "User";
        /// <summary>
        /// 微博信息
        /// </summary>
        public static string Publish = "Publish";
        /// <summary>
        /// 好友关系
        /// </summary>
        public static string Relation = "Relation";
        /// <summary>
        /// 微博评论 回复
        /// </summary>
        public static string Comment = "Comment";

        /// <summary>
        /// 微博私信
        /// </summary>
        public static string ShortMessage = "ShortMessage";

        /// <summary>
        /// 微博投票
        /// </summary>
        public static string Ticket = "Ticket";

        /// <summary>
        /// 投票
        /// </summary>
        public static string ChooseTicket = "ChooseTicket";
    }
}
