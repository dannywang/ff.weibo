﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using Mongo;

namespace Web.Admin.pages
{
    public partial class AddAllUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 批量添加用户 从Excel中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button2_Click(object sender, EventArgs e)
        {
            string file = Path.GetFileName(this.fileNewUser.PostedFile.FileName.Substring(0, this.fileNewUser.PostedFile.FileName.Length - 4));
            string filename = "";
            string ex = System.IO.Path.GetExtension(this.fileNewUser.PostedFile.FileName).ToLower();
            if (ex == ".xls")
            {
                if (this.fileNewUser.PostedFile.ContentLength > 5 * 1024 * 1024)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<script>alert('附件大小超出5MB，请压缩后上传！')</script>");
                    return;
                }
                filename = file + DateTime.Now.ToString("yyyyMMddHHmmss") + ex;
                fileNewUser.PostedFile.SaveAs(Server.MapPath("../../NewUserListUpload/") + filename);


            }
            else
            {
                Response.Write("<script>alert('请上传xls文件类型！');</script>");
                return;
            }
            string RootDir = Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath.ToString());//获取程序根目录
            string fileURL = RootDir + "NewUserListUpload\\" + filename;
            string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileURL + ";" + "Extended Properties='Excel 8.0'";
            DataSet ds = new DataSet();
            OleDbDataAdapter oada = new OleDbDataAdapter("select * from [Sheet1$]", strConn);
            oada.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string newEmail = ds.Tables[0].Rows[i]["Email"].ToString().Trim();
                    User newUser = new User();
                    newUser.LoginName = newEmail;
                    //newUser.Description = "AdminSignupBatch";
                    newUser.NickName = "";
                    newUser.Password = "000000";
                    newUser.RegisterDate = DateTime.Now;
                    newUser.Status = 0;
                    newUser.Face = "Face/default.gif";
                    AutoAddFriend(DBTools.SignupNewUser(newUser));
                }
                Response.Write("<script>alert('批量注册成功！');</script>");
            }
            else
            {
                Response.Write("<script>alert('文件无数据！');</script>");
            }

        }

        /// <summary>
        /// 自动关注特权用户
        /// </summary>
        /// <param name="email"></param>
        public void AutoAddFriend(User newUser)
        {

            List<User> AllUser = DBTools.GetAllUser();
            foreach (User item in AllUser)
            {
                if (item.Status == 1)//vip
                {
                    //新用户与vip加关注
                    Relation re = new Relation();
                    re.AddDate = DateTime.Now;
                    re.Station = 0;
                    re.LoginName = newUser.LoginName;
                    re.NickName = newUser.NickName;
                    re.RelationUser = item;
                    re.RelationUserId = item._id.ToString();
                    re.UserId = newUser._id.ToString();
                    DBTools.AddFriend(re);
                    //vip与新用户加关注
                    Relation re2 = new Relation();
                    re2.AddDate = DateTime.Now;
                    re2.Station = 0;
                    re2.LoginName = item.LoginName;
                    re2.NickName = item.NickName;
                    re2.RelationUser = newUser;
                    re2.RelationUserId = newUser._id.ToString();
                    re2.UserId = item._id.ToString();
                    DBTools.AddFriend(re2);
                }
            }

        }
    }
}