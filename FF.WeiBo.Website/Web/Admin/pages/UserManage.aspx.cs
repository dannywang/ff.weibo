﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using Mongo;

namespace Web.Admin.pages
{
    public partial class UserManage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetData();
        }
        public void GetData()
        {
            List<User> list = DBTools.GetAllUser();
            Repeater1.DataSource = list;
            Repeater1.DataBind();
        }

        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string uid = e.CommandArgument.ToString();
            User UpdateUser = new User();
            UpdateUser.LoginName = uid;
            if (e.CommandName == "setLeader")
            {
                UpdateUser.Status = 1;
                DBTools.UpdateUserStatus(UpdateUser);
            }
            if (e.CommandName == "setUser")
            {
                UpdateUser.Status = 0;
                DBTools.UpdateUserStatus(UpdateUser);
            }
            if (e.CommandName == "disable")
            {
                UpdateUser.Status = -1;
                DBTools.UpdateUserStatus(UpdateUser);
            }
            if (e.CommandName == "RePassword")
            {
                UpdateUser.Password = "000000";
                DBTools.UpdateUserPassword(UpdateUser);
            }
            Page.ClientScript.RegisterClientScriptBlock(GetType(), "", "<script>alert('设置成功！');</script>");
            GetData();
        }

        public string GetStatus(string statusCode)
        {
            string result = "";
            if (statusCode=="0")
            {
                result = "正常用户";
            }
            if (statusCode == "-1")
            {
                result = "已禁用";
            }
            if (statusCode == "1")
            {
                result = "领导用户";
            }
            return result;

        }
    }
}