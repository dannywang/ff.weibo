﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddAllUser.aspx.cs" Inherits="Web.Admin.pages.AddAllUser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="../../CSS/type.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/admin.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
     
     <p class="AdminTitle">
        批量添加新用户
    </p>
    <div class="block">
        <p class="line">
            批量添加新用户：</p>
        上传excel列表：
        <asp:FileUpload ID="fileNewUser" runat="server" />
        &nbsp;&nbsp;
        <asp:Button ID="Button2" runat="server" Text="批量添加" OnClick="Button2_Click" />
    </div>
    </form>
</body>
</html>
