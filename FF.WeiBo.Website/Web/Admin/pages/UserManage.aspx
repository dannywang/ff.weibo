﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserManage.aspx.cs" Inherits="Web.Admin.pages.UserManage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../../CSS/admin.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <p class="line">
            用户管理：
        </p>
        <table cellpadding="0" cellspacing="1" border="0" class="ManagerTable">
            <tr class="TableTitle">
                <td>用户昵称</td>
                <td>登录名（邮箱）</td>
                <td>用户状态</td>
                <td>更改权限</td>
                <td>重置密码</td>
            </tr>
            <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand">
                <ItemTemplate>
                    <tr class="TableLine">
                        <td>
                            <%#((Model.User)Container.DataItem).NickName %></td>
                        <td>
                            <%#((Model.User)Container.DataItem).LoginName %>
                        </td>
                        <td>
                            <%#GetStatus(((Model.User)Container.DataItem).Status.ToString()) %></td>
                        <td>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument="<%#((Model.User)Container.DataItem).LoginName %>"
                                CommandName="setLeader">置为领导</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandArgument="<%#((Model.User)Container.DataItem).LoginName %>"
                                CommandName="setUser">置为用户</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton3" runat="server" CommandArgument="<%#((Model.User)Container.DataItem).LoginName %>"
                                CommandName="disable">禁用</asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="LinkButton4" runat="server" CommandArgument="<%#((Model.User)Container.DataItem).LoginName %>"
                                CommandName="RePassword">重置密码</asp:LinkButton></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    </form>
</body>
</html>
