﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mongo;

namespace Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string loginName = this.loginName.Value.ToString().Trim();
            string password = this.password.Value.ToString().Trim();
            if (string.IsNullOrEmpty(loginName) || string.IsNullOrEmpty(password))
            {
                message.Text = "请输入用户名密码！";
                return;
            }
            //管理员登陆
            if (loginName == "admin" && password=="admin")
            {
                Response.Redirect("Admin/main.html");
                return;
            }
            if (!DBTools.IsEmail(loginName))
            {
                message.Text = "请输入正确的邮箱格式！";
                return;
            }
            Model.User LoginUser = DBTools.UserLogin(loginName, password);
            if (LoginUser != null)//success
            {
                if (LoginUser.Status == -1)
                {
                    message.Text = LoginUser.LoginName + " 用户已禁用！";
                    return;
                }
                Response.Cookies["UserLoginName"].Value = LoginUser.LoginName;
                Response.Cookies["UserID"].Value = LoginUser._id.ToString();
                Session["CurrentUser"] = LoginUser;
                Response.Redirect("Customer/home.aspx");
            }
            else
            {
                message.Text = "用户名或密码错误！";
            }
        }


    }
}