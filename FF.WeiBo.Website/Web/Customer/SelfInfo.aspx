﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelfInfo.aspx.cs" Inherits="Web.Customer.SelfInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../CSS/login.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <h1 style="margin-bottom: 20px;">个人信息设置</h1>
    <p class="Error">
        <asp:Label ID="message" runat="server" Text=""></asp:Label>
    </p>
    <p>
        <label for="nickName">
            微博昵称：
        </label>
        <input type="text" id="nickName" runat="server" style="width: 200px;">
    </p>
    <p>
        <label for="desp">
            个人描述：
        </label>
        <input type="text" id="desp" runat="server" style="width: 200px;">
    </p>
    <p>
        <asp:LinkButton ID="btnUpdate" runat="server" CssClass="submit" OnClick="btnUpdate_Click">更新信息</asp:LinkButton>
        <asp:LinkButton ID="btnReturn" runat="server" CssClass="submit2" OnClick="btnReturn_Click">返回首页</asp:LinkButton>
    </p>
    </form>
</body>
</html>
