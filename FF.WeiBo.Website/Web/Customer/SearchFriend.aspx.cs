﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mongo;
using Model;

namespace Web.Customer
{
    public partial class SearchFriend : System.Web.UI.Page
    {
        public string NickName = string.Empty;
        public string UserId = "";
        public User user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                user = (User)Session["CurrentUser"];
                UserId = user._id.ToString();
                NickName = user.NickName;
                if (Request.QueryString["key"] != null)
                {
                    string key = Request.QueryString["key"].ToString();
                    txtKey.Text = key;
                    List<User> list = DBTools.SearchUserByKey(key);
                    Repeater1.DataSource = list;
                    Repeater1.DataBind();
                }
            }

        }

        protected void Search_Click(object sender, EventArgs e)
        {
            string key = txtKey.Text.Trim();
            List<User> list = DBTools.SearchUserByKey(key);
            Repeater1.DataSource = list;
            Repeater1.DataBind();
        }

        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Add")
            {
                string uid = e.CommandArgument.ToString();
                User u = DBTools.GetUserInfoById(uid);
                User user = (User)Session["CurrentUser"];
                Relation newRelation = new Relation();
                newRelation.UserId = Request.Cookies["UserID"].Value.ToString();
                newRelation.RelationUser = u;
                newRelation.RelationUserId = u._id.ToString();
                newRelation.Station = 0;
                newRelation.LoginName = Request.Cookies["UserLoginName"].Value.ToString();
                newRelation.NickName = user.NickName;
                newRelation.AddDate = DateTime.Now;
                DBTools.AddFriend(newRelation);
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "", "<script>alert('好友添加成功！');</script>");

            }
        }

        protected void LinkButton_exit_Click(object sender, EventArgs e)
        {
            //Response.Cookies["UserLoginName"].Expires = DateTime.Now.AddYears(-9999);
            //Response.Cookies["UserID"].Expires = DateTime.Now.AddYears(-9999);
            //Session["CurrentUser"] = null;
            HttpCookie aCookie;
            string cookieName;
            int limit = Request.Cookies.Count;
            for (int i = 0; i < limit; i++)
            {
                cookieName = Request.Cookies[i].Name;
                aCookie = new HttpCookie(cookieName);
                aCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(aCookie);
            }
            Response.Redirect("../Login.aspx");
        }
    }
}