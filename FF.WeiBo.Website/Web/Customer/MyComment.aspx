﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyComment.aspx.cs" Inherits="Web.Customer.MyComment" %>

<%@ Register Src="TopBar.ascx" TagName="TopBar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../CSS/type.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/customer.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1-vsdoc.js"></script>
    <script type="text/javascript">
        function ShowRePanel(cid) {
            $("#panel_" + cid).slideToggle();
            //$("#btnRe_" + cid).click(function () {
            //    var remsg=$("#msg_" + cid).val();
            //    $.post("AjaxProcessPage.aspx?t=" + new Date(), { type: "ReComment",commentId:cid,reMessage:remsg }, function (data) {
            //        if (data.length > 0) { }
            //    });
            //});
        }

        //评论微博
        function CommentWeibo(comId, weiboId) {
            var commentTxt = $("#msg_" + weiboId).val();
            //ajax post
            $.post("AjaxProcessPage.aspx?t=" + new Date(), { type: "ReCommentWeibo", msg: commentTxt, comId: comId }, function (data) {
                if (data.toString() == "OK") {
                    alert('回复成功！');
                }
                else {
                    alert('回复失败');
                }
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <uc1:TopBar ID="TopBar1" runat="server" />
    <div class="ManageBack">
        <p class="fTitle">我收到的评论</p>
        <asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                <div class="commentlistItem">
                    <div style="width: 50px; height: 50px; float: left;">
                        <img src="<%#((Model.Comment)Container.DataItem).PublishUser.Face %>" style="width: 50px;
                            height: 50px;" />
                    </div>
                    <div style="width: 500px; height: 50px; float: left; margin-left: 20px;">
                        <p style="margin: 6px; font-size: 12px; font-weight: bold; padding-left: 10px;">
                            <a href="FriendIndex.aspx?uid=<%#((Model.Comment)Container.DataItem).PublishUser._id %>"
                                target="_blank">
                                <%#((Model.Comment)Container.DataItem).PublishUser.NickName %></a>&nbsp;回复
                            <%=currentUser.NickName %>：
                        </p>
                        <p style="margin-left: 30px; margin-top: 10px; font-size: 13px;">
                            <%#((Model.Comment)Container.DataItem).CommentMessage %>
                        </p>
                        <p style="margin-left: 30px; color: #666; margin-top: 14px;">
                            回复我的<%#((Model.Comment)Container.DataItem).CommentWeibo==null?"的评论："+GetSourceInfo(((Model.Comment)Container.DataItem).TargetId).CommentMessage:"微博："+((Model.Comment)Container.DataItem).CommentWeibo.Message%><span style="float: right"><a
                                href="#" onclick="ShowRePanel('<%#((Model.Comment)Container.DataItem)._id.ToString() %>')">回复
                            </a></span>
                        </p>
                    </div>
                    <div class="reComment" id="panel_<%#((Model.Comment)Container.DataItem)._id.ToString() %>">
                        <p>
                            <input type="text" style="width: 370px; margin: 10px;" id="msg_<%#((Model.Comment)Container.DataItem)._id.ToString() %>" />
                        </p>
                        <p style="text-align: right">
                            <input type="button" class="btn" style="width: 70px; margin-right: 10px;" value="回复"
                                id="btnRe_<%#((Model.Comment)Container.DataItem)._id.ToString() %>" onclick="CommentWeibo('<%#((Model.Comment)Container.DataItem)._id.ToString()%>','<%#((Model.Comment)Container.DataItem)._id.ToString() %>')" />
                        </p>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    </form>
</body>
</html>
