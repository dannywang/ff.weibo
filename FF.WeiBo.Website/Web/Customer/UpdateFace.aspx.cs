﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Model;
using Mongo;

namespace Web.Customer
{
    public partial class UpdateFace : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (this.FileUpload1.PostedFile.FileName != "")
            {
                //string file = this.FileUpload1.PostedFile.FileName.Substring(0,this.FileUpload1.PostedFile.FileName.Length-4)+"_"+System.DateTime.Now.ToString("yyyyMMddHHmmss");
                string file = Path.GetFileName(this.FileUpload1.PostedFile.FileName.Substring(0, this.FileUpload1.PostedFile.FileName.Length - 4) + "_" + System.DateTime.Now.ToString("yyyyMMddHHmmss"));

                string ex = System.IO.Path.GetExtension(this.FileUpload1.PostedFile.FileName).ToLower();
                if (ex == ".jpg" || ex == ".jpeg" || ex == ".png" || ex == ".bmp" || ex == ".gif")
                {
                    if (this.FileUpload1.PostedFile.ContentLength > 1 * 1024 * 1024)
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<script>alert('文件大小超出1MB，请压缩后上传！')</script>");
                        return;
                    }
                    FileUpload1.PostedFile.SaveAs(Server.MapPath("Face/") + file + ex);

                }
                else
                {
                    Response.Write("<script>alert('请上传本系统所支持的文件类型！');</script>");
                    return;
                }
                string faceURL = "Face/" + file + ex;

                User user = new User();
                user.Face = faceURL;
                user.LoginName = Request.Cookies["UserLoginName"].Value.ToString();
                DBTools.UpdateUserFace(user);

                user = DBTools.GetUserNewInfo(Request.Cookies["UserLoginName"].Value.ToString());
                Session["CurrentUser"] = user;
                message.Text = "头像更新成功";
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");
        }
    }
}