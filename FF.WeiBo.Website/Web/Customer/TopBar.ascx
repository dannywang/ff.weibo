﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopBar.ascx.cs" Inherits="Web.Customer.TopBar" %>
<div id="titleBar">
    <ul class="titleBarUl">
        <li class="titleBarLiItem" id="title_logo"></li>
        <li class="titleBarLiItem"><a href="home.aspx">首 页</a></li>
        <li class="titleBarLiItem"><a href="Me.aspx?uid=<%=UserId %>">
            <%=NickName %></a></li>
        <li class="titleBarLiItem" id="title_search">
            <div style="width: 200px;">
            </div>
        </li>
        <li class="titleBarLiItem"><a href="SelfInfo.aspx">账户信息</a></li>
        <li class="titleBarLiItem"><a href="UpdatePassword.aspx">修改密码</a></li>
        <li class="titleBarLiItem">
            <asp:LinkButton ID="LinkButton_exit" runat="server" OnClientClick="return confirm('确认退出？')">退出</asp:LinkButton>
        </li>
    </ul>
</div>
