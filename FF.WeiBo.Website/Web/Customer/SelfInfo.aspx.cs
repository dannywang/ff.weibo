﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using Mongo;
using MongoDB;
using MongoDB.Bson;

namespace Web.Customer
{
    public partial class SelfInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                User u = new User();
                u = DBTools.GetUserInfo(Request.Cookies["UserLoginName"].Value.ToString());
                this.nickName.Value = u.NickName;
                this.desp.Value = u.Description;
            }

        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            User user = new User();
            user.NickName = this.nickName.Value.ToString().Trim();
            user.Description = this.desp.Value.ToString().Trim();
            user.LoginName = Request.Cookies["UserLoginName"].Value.ToString();
            DBTools.UpdateUserInfo(user);
            message.Text = "更新成功";
            user = DBTools.GetUserNewInfo(Request.Cookies["UserLoginName"].Value.ToString());
            Session["CurrentUser"] = user;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");
        }
    }
}