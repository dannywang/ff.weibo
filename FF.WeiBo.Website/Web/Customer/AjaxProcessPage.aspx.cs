﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mongo;
using Model;

namespace Web.Customer
{
    public partial class AjaxProcessPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Get
            if (Request.QueryString["type"] != null)
            {
                string type = Request.QueryString["type"].ToString();
                switch (type)
                {
                    case "CheckNewUser":
                        {
                            Response.Write("ok");
                        }
                        //  else
                        {
                            Response.Write("error");
                        }
                        break;
                    case "RegisterNewUser":
                        {
                            Response.Write("ok");
                        }
                        //   else
                        {
                            Response.Write("error");
                        }
                        break;
                    case "UserLogin":
                        {
                            Response.Write("ok");
                        }
                        //   else
                        {
                            Response.Write("error");
                        }
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #region Post
            if (Request.Form["type"] != null)
            {
                string type = Request.Form["type"].ToString();
                switch (type)
                {
                    case "CommentWeibo":
                        {
                            CommentWeibo(Request.Form["msg"].ToString(), Request.Form["weiboId"].ToString());
                        }
                        break;
                    case "ReCommentWeibo":
                        {
                            ReCommentWeibo(Request.Form["msg"].ToString(), Request.Form["comId"].ToString());
                        }
                        break;
                    case "GetComment":
                        {
                            GetPublishedCommentByWeiboId(Request.Form["weiboId"].ToString());
                        }
                        break;
                    case "GetNewMessage":
                        {
                            GetNewMessage();
                        }
                        break;
                    case "DeleteWeiboByVip":
                        {
                            DeleteWeiboByVip(Request.Form["weiboId"].ToString());
                        }
                        break;
                    case "ReadMessageByType":
                        {
                            ReadMessageByType(Request.Form["typeName"].ToString());
                        }
                        break;
                    case "ReComment":
                        {
                            ReComment(Request.Form["commentId"].ToString(), Request.Form["reMessage"].ToString());
                        }
                        break;

                    case "CreateTicket":
                        {
                            Ticket t = new Ticket();
                            t = DBTools.JosnDeserialize<Ticket>(Request.Form["newTicket"].ToString(), null);

                            CreateTicket(t);
                        }
                        break;
                    case "AddTicket":
                        {
                            AddTicket(Request.Form["value"].ToString(), Request.Form["tid"].ToString());
                        }
                        break;
                    case "GetTicketResult":
                        {
                            GetTicketResult(Request.Form["tid"].ToString());
                        }
                        break;
                    case "CheckTicket":
                        {
                            CheckTicket(Request.Form["tid"].ToString());
                        }
                        break;
                        
                }
            }
            #endregion
        }

        public void CommentWeibo(string message, string weiboId)
        {
            Comment com = new Comment();
            com.AddDate = DateTime.Now;
            com.CommentMessage = message;
            com.CommentWeibo = DBTools.GetPublishedWeiBoById(weiboId);
            com.CommentWeiboId = weiboId;
            com.Status = 0;
            com.PublishUser = (User)Session["CurrentUser"];
            com.PublishUserId = ((User)Session["CurrentUser"])._id.ToString();
            com.TargetId = "0";
            DBTools.PublshComment(com);
            Response.Write("OK");
        }

        public void ReCommentWeibo(string message, string comId)
        {
            Comment com = new Comment();
            com.AddDate = DateTime.Now;
            com.CommentMessage = message;
            //com.CommentWeibo = DBTools.GetPublishedWeiBoById(weiboId);
            //com.CommentWeiboId = weiboId;
            com.Status = 0;
            com.PublishUser = (User)Session["CurrentUser"];
            com.PublishUserId = ((User)Session["CurrentUser"])._id.ToString();
            com.TargetId = comId;
            DBTools.PublshComment(com);
            Response.Write("OK");
        }

        public void GetPublishedCommentByWeiboId(string weiboId)
        {
            List<Comment> commentList = DBTools.GetPublishedCommentByWeiboId(weiboId);
            List<Comment> commentListSort = new List<Comment>();
            // 根据发布时间进行倒序排序
            commentListSort = commentList.OrderByDescending(p => p.AddDate).ToList();
            string jsonResult = DBTools.JsonSerialize(commentListSort);
            Response.Write(jsonResult);
        }

        public void GetNewMessage()
        {
            if (Context.Request.Cookies["UserID"] != null)
            {
                string uid = Request.Cookies["UserID"].Value.ToString();
                string loginName = DBTools.GetUserInfoById(uid).LoginName;
                string json = DBTools.GetNewMessageByUserId(uid, loginName);
                Response.Write(json);
            }
            else
            {
                Response.Write("Error");
            }
        }

        public void DeleteWeiboByVip(string id)
        {
            DBTools.DeleteWeiboById(id);
            Response.Write("OK");
        }

        public void ReadMessageByType(string typeName)
        {
            if (typeName.ToString() == "shortMessage")
            {
                if (Context.Request.Cookies["UserID"] != null)
                {
                    string uid = Request.Cookies["UserID"].Value.ToString();
                    DBTools.UpdateShortMessageStatus(uid);
                }
                else
                {
                    Response.Write("Error");
                }
            }
            if (typeName.ToString() == "comment")
            {
                if (Context.Request.Cookies["UserID"] != null)
                {
                    string uid = Request.Cookies["UserID"].Value.ToString();
                    DBTools.UpdateCommentStatus(uid);
                }
                else
                {
                    Response.Write("Error");
                }
            }
        }

        public void ReComment(string cid, string msg)
        {
            if (Context.Request.Cookies["UserID"] != null)
            {
                string uid = Request.Cookies["UserID"].Value.ToString();
                DBTools.ReComment(uid, cid, msg);
            }
            else
            {
                Response.Write("Error");
            }
        }

        public void CreateTicket(Ticket t)
        {
            if (Context.Request.Cookies["UserID"] != null)
            {
                string uid = Request.Cookies["UserID"].Value.ToString();
                User u = DBTools.GetUserInfoById(uid);
                t.pubUser = u;
                t.pubUserLoginName = u.LoginName;
                t.addDate = DateTime.Now;
                t.tid = DateTime.Now.ToString("yyyyMMddHHmmss");
                DBTools.CreateTicket(t);
                WeiBo wb = new WeiBo();
                wb.Message = u.NickName + "发布了新投票：" + "<a href='TicketShow.aspx?tid=" + t.tid + "'>" + t.title + "</a>";
                Files f = new Files();
                wb.File = f;
                wb.Owner = u;
                wb.PublishDate = DateTime.Now;
                wb.Status = 9;
                wb.Theme = "0";
                wb.Writer = u.NickName;
                wb.WriterId = u._id.ToString();
                DBTools.PublshNewWeiBo(wb);
                Response.Write("OK");
            }
        }

        public void AddTicket(string value, string tid)
        {
            string uid = Request.Cookies["UserID"].Value.ToString();
            User u = DBTools.GetUserInfoById(uid);
            ChooseTicket newChoose = new ChooseTicket();
            newChoose.ChooseItem = value;
            newChoose.ChooseUser = u;
            newChoose.ChooseUserLoginName = u.LoginName;
            newChoose.TargetTicket = DBTools.GetTicketByTid(tid);
            newChoose.TargetTid = tid;
            DBTools.AddUserTicket(newChoose);
            Response.Write("OK");
        }

        public void GetTicketResult(string tid)
        {
            List<ChooseTicket> list = new List<ChooseTicket>();
            list = DBTools.GetTicketResultByTid(tid);
            var q =
                from p in list
                group p by p.ChooseItem into g
                select new
                {
                    g.Key,
                    Num = g.Count()
                };

            List<Model.TicketShow> result = new List<Model.TicketShow>();
            //Dictionary<string, int> result = new Dictionary<string, int>();
            foreach (var i in q)
            {
                Model.TicketShow nn = new Model.TicketShow();
                nn.name = i.Key.ToString();
                nn.value = i.Num;
                result.Add(nn);
            }
            Response.Write(DBTools.JsonSerialize(result));
        }

        public void CheckTicket(string tid)
        {
             string uid = Request.Cookies["UserID"].Value.ToString();
            User u = DBTools.GetUserInfoById(uid);
            if (DBTools.CheckTicket(tid, u.LoginName))
            {
                Response.Write("OK");
            }
            else
            {
                Response.Write("error");
            }
        }

    }
}