﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mongo;
using Model;

namespace Web.Customer
{
    public partial class Me : System.Web.UI.Page
    {
        public User user;
        public string NickName = "";
        public string puid = "";
        public User currentUser;
        public int PublishedCount;
        public int FriendListCount;

        protected void Page_Load(object sender, EventArgs e)
        {
            string uid = Request.QueryString["uid"].ToString();
            puid = uid;
            DBTools.GetUserInfoById(uid);
            user = DBTools.GetUserInfoById(uid); //(User)Session["CurrentUser"];
            currentUser = (User)Session["CurrentUser"];

            if (!IsPostBack)
            {
                BindData();
            }
        }

        public void BindData()
        {
            //绑定微博信息
            List<WeiBo> DataList = new List<WeiBo>();
            DataList = DBTools.GetPublishedWeiBo(user._id.ToString());
            if (DataList.Count == 0)
            {
                NewUserShow.Visible = true;
            }
            Repeater_PubData.DataSource = DataList;
            Repeater_PubData.DataBind();
            //绑定用户基本信息
            userFace.Src = user.Face == null ? "Face/default.gif" : user.Face;
            lblNickName.Text = user.NickName;
            lblDesp.Text = user.Description;
            NickName = user.NickName;
            //绑定私信信息
            List<ShortMessage> messageList = new List<ShortMessage>();
            currentUser = (User)Session["CurrentUser"];
            messageList = DBTools.GetShortMessageByLoginName(currentUser.LoginName);
            Repeater_ReMessage.DataSource = messageList;
            Repeater_ReMessage.DataBind();
            //获取已经发布的微博数
            PublishedCount = DataList.Count;
            //获取关注的好友数目
            List<User> FriendList = DBTools.GetUserFriendListByUId(user._id.ToString());
            FriendListCount = FriendList.Count;
        }

        protected void ddl_theme_SelectedIndexChanged(object sender, EventArgs e)
        {
            int theme = int.Parse(ddl_theme.SelectedItem.Value);
            if (theme == 0)
            {
                BindData();
            }
            else
            {
                List<WeiBo> DataList = new List<WeiBo>();
                DataList = DBTools.GetPublishedWeiBoByTheme(user._id.ToString(), theme);
                Repeater_PubData.DataSource = DataList;
                Repeater_PubData.DataBind();

                userFace.Src = user.Face == null ? "Face/default.gif" : user.Face;
                lblNickName.Text = user.NickName;
                lblDesp.Text = user.Description;
                NickName = user.NickName;
            }
        }

        protected void Repeater_PubData_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.ToString() == "delete")
            {
                string weiboId = e.CommandArgument.ToString();
                DBTools.DeleteWeiboById(weiboId);
                BindData();
            }
        }

        protected void LinkButton_exit_Click(object sender, EventArgs e)
        {
            HttpCookie aCookie;
            string cookieName;
            int limit = Request.Cookies.Count;
            for (int i = 0; i < limit; i++)
            {
                cookieName = Request.Cookies[i].Name;
                aCookie = new HttpCookie(cookieName);
                aCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(aCookie);
            }
            Response.Redirect("../Login.aspx");
        }

        /// <summary>
        /// 回复私信
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRe_Click(object sender, EventArgs e)
        {
            string msg = txtReMessage.Text.Trim();
            ShortMessage reMessage = new ShortMessage();
            reMessage.AddDate = DateTime.Now;
            reMessage.Message = msg;
            reMessage.PublishUser = currentUser;
            reMessage.PublishUserLoginName = currentUser.LoginName;
            reMessage.ReceiveUser = DBTools.GetUserInfo(hdUid.Value.ToString());
            reMessage.ReceiveUserLoginName = DBTools.GetUserInfo(hdUid.Value.ToString()).LoginName;
            reMessage.Status = 0;
            DBTools.SendMessage(reMessage);
            Page.ClientScript.RegisterClientScriptBlock(GetType(), "", "<script>alert('回复成功！');</script>");
            txtReMessage.Text = "";
        }
    }
}