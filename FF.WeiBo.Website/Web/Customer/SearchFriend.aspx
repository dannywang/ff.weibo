﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchFriend.aspx.cs" Inherits="Web.Customer.SearchFriend" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>我的主页</title>
    <link href="../CSS/type.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/customer.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="titleBar">
        <ul class="titleBarUl">
            <li class="titleBarLiItem" id="title_logo"></li>
            <li class="titleBarLiItem"><a href=home.aspx>首 页</a></li>
            <li class="titleBarLiItem"><a href="Me.aspx?uid=<%=UserId %>">
                <%=NickName %></a></li>
            <li class="titleBarLiItem" id="title_search">
                <div style="width: 200px;">
                    搜索好友</div>
            </li>
            <li class="titleBarLiItem"><a href="SelfInfo.aspx">账户信息</a></li>
            <li class="titleBarLiItem">  <asp:LinkButton ID="LinkButton_exit" runat="server" OnClick="LinkButton_exit_Click"
                    OnClientClick="return confirm('确认退出？')">退出</asp:LinkButton></li>
        </ul>
    </div>
       
    <div class="BackWall">

    <div class="ManageBack">
        <p class="fTitle">搜索好友</p>
        <div class="searchBar">
            <asp:TextBox ID="txtKey" runat="server" Width="324px"></asp:TextBox>
            &nbsp;
            <asp:Button ID="Search" runat="server" Text="搜 索" OnClick="Search_Click" />
        </div>
        <div class="SearchContent">
          
            <asp:Repeater ID="Repeater1" runat="server" 
                onitemcommand="Repeater1_ItemCommand">
            <ItemTemplate>
              <div class="searchItem">
                <div style="width: 50px; height: 50px; float: left;">
                    <img src="<%#((Model.User)Container.DataItem).Face %>"  style="width: 50px; height: 50px;" />
                </div>
                <div style="width: 300px; height: 50px; float: left; margin-left: 20px;">
                    <p style="margin: 6px; font-size: 14px; font-weight: bold; padding-left: 10px;">
                     <%#((Model.User)Container.DataItem).NickName %></p>
                    <p style="margin-left: 30px; color: #666; margin-top: 4px;">
                        <%#((Model.User)Container.DataItem).Description %> </p>
                </div>
                <div style="width: 70px; height: 50px; float: left; margin-left: 10px;">
                    <p style="margin-top: 20px;">
                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Add" CommandArgument="<%#((Model.User)Container.DataItem)._id%>">加关注</asp:LinkButton></p>
                </div>
            </div>
            </ItemTemplate>
            </asp:Repeater>
             
        </div>
    </div>


        
      
    </div>
    </form>
</body>
</html>
