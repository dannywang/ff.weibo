﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using Mongo;

namespace Web.Customer
{
    public partial class FriendIndex : System.Web.UI.Page
    {
        public User user;
        public string NickName = "";
        public string puid = "";
        public User currentUser;
        public int FriendListCount;
        public int PublishedCount;
        protected void Page_Load(object sender, EventArgs e)
        {
            string uid = Request.QueryString["uid"].ToString();
            puid = uid;
            DBTools.GetUserInfoById(uid);
            user = DBTools.GetUserInfoById(uid); //(User)Session["CurrentUser"];
            currentUser = (User)Session["CurrentUser"];
            TopBar1.UserId = currentUser._id.ToString();
            TopBar1.NickName = currentUser.NickName;
            if (!IsPostBack)
            {
                BindData();
            }
        }

        public void BindData()
        {
            //绑定微博信息
            List<WeiBo> DataList = new List<WeiBo>();
            DataList = DBTools.GetPublishedWeiBo(user._id.ToString());
            Repeater_PubData.DataSource = DataList;
            Repeater_PubData.DataBind();
            PublishedCount = DataList.Count;
            //绑定用户基本信息
            userFace.Src = user.Face == null ? "Face/default.gif" : user.Face;
            lblNickName.Text = user.NickName;
            lblDesp.Text = user.Description;
            NickName = user.NickName;
            //获取好友列表
            List<User> FriendList = DBTools.GetUserFriendListByUId(user._id.ToString());
            //获取关注的好友数目
            FriendListCount = FriendList.Count;
        }

        protected void LinkButton_exit_Click(object sender, EventArgs e)
        {
            HttpCookie aCookie;
            string cookieName;
            int limit = Request.Cookies.Count;
            for (int i = 0; i < limit; i++)
            {
                cookieName = Request.Cookies[i].Name;
                aCookie = new HttpCookie(cookieName);
                aCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(aCookie);
            }
            Response.Redirect("../Login.aspx");
        }

        protected void btnPubMesage_Click(object sender, EventArgs e)
        {
            string msg = txtMessage.Text.Trim();
            if (string.IsNullOrEmpty(msg))
            {
                return;
            }
            ShortMessage newMsg = new ShortMessage();
            newMsg.AddDate = DateTime.Now;
            newMsg.Message = msg;
            newMsg.PublishUser = currentUser;
            newMsg.PublishUserLoginName = currentUser.LoginName;
            newMsg.ReceiveUser = user;
            newMsg.ReceiveUserLoginName = user.LoginName;
            newMsg.Status = 0;

            DBTools.SendMessage(newMsg);
            txtMessage.Text = "";
            Page.ClientScript.RegisterClientScriptBlock(GetType(), "", "<script>alert('私信发送成功！');</script>");
            return;
        }
    }
}