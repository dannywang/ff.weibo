﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using Mongo;
using MongoDB;
using MongoDB.Bson;

namespace Web.Customer
{
    public partial class UpdatePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            User u = new User();
            u = DBTools.GetUserInfo(Request.Cookies["UserLoginName"].Value.ToString());
            if (old.Value.Trim() != u.Password)
            {
                message.Text = "原密码错误，请重新输入";
                return;
            }
            if (new1.Value != new2.Value)
            {
                message.Text = "新密码不一致，请重新输入";
                return;
            }
            if (string.IsNullOrEmpty(new1.Value.Trim()) || string.IsNullOrEmpty(new2.Value.Trim()))
            {
                message.Text = "请重新输入";
                return;
            }

            User user = new User();
            user.Password = new1.Value.Trim();
            user.LoginName = Request.Cookies["UserLoginName"].Value.ToString();
            DBTools.UpdateUserPassword(user);
            message.Text = "更新成功";
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");
        }
    }
}