﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using Mongo;

namespace Web.Customer
{
    public partial class home : System.Web.UI.Page
    {
        public User user;
        public string NickName = string.Empty;
        public string UserId = "";
        public int PublishedCount;
        public int FriendListCount;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserLoginName"] != null && Request.Cookies["UserID"] != null && Request.Cookies["UserLoginName"].Value != null && Request.Cookies["UserID"].Value != null)
            {
                user = (User)Session["CurrentUser"];
                UserId = Request.Cookies["UserID"].Value.ToString();
                if (user.NickName == null || user.Description == null)
                {
                    
                    Page.ClientScript.RegisterClientScriptBlock(GetType(), "", "<script>alert('请先填写昵称和描述');window.location='SelfInfo.aspx'</script>");
                    return;
                }
                BindData();
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "", "<script>alert('非法用户！请登录');window.location='../Login.aspx'</script>");
                return;
            }
        }
        /// <summary>
        /// 发布微博
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Btn_Publish_Click(object sender, EventArgs e)
        {
            WeiBo wb = new WeiBo();
            Files f = new Files();
            if (string.IsNullOrEmpty(txtMessage.Text.Trim()))
            {
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "", "<script>alert('请输入微博内容！');</script>");
           
                CheckBox1.Checked = false;
                return;
            }
            if (CheckBox1.Checked)
            {
                if (this.FileUpload1.PostedFile.FileName != "")
                {
                    string file = this.FileUpload1.PostedFile.FileName.Substring(0, this.FileUpload1.PostedFile.FileName.Length - 4) + "_" + System.DateTime.Now.ToString("yyyyMMddHHmmss");
                    //string file = Path.GetFileName(this.FileUpload1.PostedFile.FileName.Substring(0, this.FileUpload1.PostedFile.FileName.Length - 4));

                    string ex = System.IO.Path.GetExtension(this.FileUpload1.PostedFile.FileName).ToLower();
                    if (ex == ".rar" || ex == ".jpg" || ex == ".png" || ex == ".gif")
                    {
                        if (this.FileUpload1.PostedFile.ContentLength > 2 * 1024 * 1024)
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "", "<script>alert('附件大小超出2MB，请压缩后上传！')</script>");
                            return;
                        }
                        FileUpload1.PostedFile.SaveAs(Server.MapPath("UserUplodeFiles/") + file + ex);

                    }
                    else
                    {
                        Response.Write("<script>alert('请上传所支持的文件类型！');</script>");
                        return;
                    }

                    //图片缩放
                    if (ex != ".rar")
                    {
                        string RootDir = Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath.ToString());//获取程序根目录
                        DBTools.MakeThumbnail(RootDir + "Customer/UserUplodeFiles/" + file + ex, RootDir + "Customer/UserUplodeFiles/" + file + "_small" + ex, 150, 150, "EQU");
                    }
                    f.FileURL = "UserUplodeFiles/" + file + ex;
                    f.FileName = this.FileUpload1.PostedFile.FileName;
                    f.FileType = ex == ".rar" ? "file" : "pic";
                    f.Size = this.FileUpload1.PostedFile.ContentLength.ToString();
                }
                else
                {
                    Response.Write("<script>alert('请上传文件！');</script>");
                    CheckBox1.Checked = false;
                    return;
                }
            }
            wb.Message = txtMessage.Text.ToString();
            wb.Writer = user.NickName;
            wb.WriterId = user._id.ToString();
            wb.Theme = ddl_theme.SelectedItem.Value;
            wb.Status = 0;
            wb.PublishDate = DateTime.Now;
            wb.File = f;
            wb.Owner = user;
            DBTools.PublshNewWeiBo(wb);
            Page.ClientScript.RegisterClientScriptBlock(GetType(), "", "<script>alert('发布成功！');</script>");
            txtMessage.Text = "";
            BindData();
            CheckBox1.Checked = false;
        }

        public void BindData()
        {
            userFace.Src = user.Face == null ? "Face/default.gif" : user.Face;
            NickName = user.NickName;
            //获取已经发布的微博数
            List<WeiBo> DataList = new List<WeiBo>();
            DataList = DBTools.GetPublishedWeiBo(user._id.ToString());
            PublishedCount = DataList.Count;
            //获取好友列表
            List<User> FriendList = DBTools.GetUserFriendListByUId(user._id.ToString());
            //获取关注的好友数目
            FriendListCount = FriendList.Count;
            //增加我自己的微博
            FriendList.Add(user);
            List<WeiBo> AllWeibo = new List<WeiBo>();
            //获取好友发布的微博列表
            for (int i = 0; i < FriendList.Count; i++)
            {
                List<WeiBo> tempList = new List<WeiBo>();
                tempList = DBTools.GetPublishedWeiBo(FriendList[i]._id.ToString());
                for (int j = 0; j < tempList.Count; j++)
                {
                    AllWeibo.Add(tempList[j]);
                }
            }
            //按照发布时间进行排序
            List<WeiBo> AllWeiboSortList = new List<WeiBo>();
            // 根据发布时间进行倒序排序
            AllWeiboSortList = AllWeibo.OrderByDescending(p => p.PublishDate).ToList();


            PagedDataSource pds = new PagedDataSource();
            pds.DataSource = AllWeiboSortList;
            pds.AllowPaging = true;
            pds.PageSize = 10;
            int curpage;
            if (Request.QueryString["page"] != null)
            {
                curpage = Convert.ToInt32(Request.QueryString["page"]);
            }
            else
            {
                curpage = 1;
            }
            pds.CurrentPageIndex = curpage - 1;
            Label1.Text = curpage.ToString();
            Label2.Text = pds.PageCount.ToString();

            int intAllNum = pds.PageCount;
            string strPager = "";
            if (intAllNum > 0)
            {
                for (int i = 0; i < intAllNum; i++)
                {
                    strPager += "<a style=\"color:#ff0000\" href ='?page=" + (i + 1) + "'>" + (i + 1) + "</a>&nbsp;";
                }
               // Label3.Text = strPager;

            }
            if (!pds.IsFirstPage)
            {
                HyperLink1.NavigateUrl = Request.CurrentExecutionFilePath + "?page=" + Convert.ToString(curpage - 1);
            }
            if (!pds.IsLastPage)
            {
                HyperLink2.NavigateUrl = Request.CurrentExecutionFilePath + "?page=" + Convert.ToString(curpage + 1);
            }


            Repeater_AllWeibo.DataSource = pds; //AllWeiboSortList;
            Repeater_AllWeibo.DataBind();
        }

        /// <summary>
        /// 搜索好友
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button2_Click(object sender, EventArgs e)
        {
            string key = txtSearch.Value.ToString().Trim();
            if (!string.IsNullOrEmpty(key))
            {
                Response.Redirect("SearchFriend.aspx?key=" + key);
            }
        }

        protected void LinkButton_exit_Click(object sender, EventArgs e)
        {
            HttpCookie aCookie;
            string cookieName;
            int limit = Request.Cookies.Count;
            for (int i = 0; i < limit; i++)
            {
                cookieName = Request.Cookies[i].Name;
                aCookie = new HttpCookie(cookieName);
                aCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(aCookie);
            }
            Response.Redirect("../Login.aspx");
        }

        public string SetPicSize(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                string[] file = url.Split('.');
                return file[0] + "_small." + file[1];
            }
            return "";
        }
    }
}