﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Web.Customer.home" %>

<%@ Register Src="MessageList.ascx" TagName="MessageList" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>我的主页</title>
    <link href="../CSS/type.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/customer.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/MessageBox.js"></script>
    <script>
        function showTxt(val) {
            if (val.toString() == "搜索微博，找人") {
                document.getElementById("txtSearch").value = '';
            }
            if (val.toString() == "") {
                document.getElementById("txtSearch").value = '搜索微博，找人';
            }
        }
        //初始化评论微博
        function InitComment(weiboId) {
            if ($("#replyTxt_" + weiboId)[0].style.display == "none") {
                $("#replyTxt_" + weiboId).show();

                $("#commit_" + weiboId).css("padding", "20px");
                //显示历史评论列表
                $.post("AjaxProcessPage.aspx?t=" + new Date(), { type: "GetComment", weiboId: weiboId }, function (data) {
                    if (data.length > 0) {
                        var comJSON = eval("(" + data + ")");
                        //构造容器DIV的内容模板
                        var contents = "";

                        for (var i = 0; i < comJSON.length; i++) {
                            var contentItem = "<div class=\"CommitedItems\"><div class=\"commitLeft\">";
                            contentItem += "<img src=\"" + comJSON[i].PublishUser.Face + "\" width=\"30px\" height=\"30px\" />";
                            contentItem += "</div><div class=\"CommitRight\"><p class=\"commitItems\">";
                            contentItem += comJSON[i].PublishUser.NickName + ": &nbsp;" + comJSON[i].CommentMessage;
                            contentItem += "</p><div class=\"CommitReply\">回复</div></div></div>";

                            contents += contentItem;
                            contentItem = '';
                        }
                        $("#CommitedContainer_" + weiboId).html(contents);
                        $("#CommitedContainer_" + weiboId).show();
                        //////////////////////
                    }
                });
            }
            else {
                $("#replyTxt_" + weiboId).hide();
                $("#CommitedContainer_" + weiboId).hide();
                $("#commit_" + weiboId).css("padding", "0px");
            }
            $(".Commit").css("float", ""); //IE CSS bug
            $(".Commit").css("float", "right"); //IE CSS bug

        }
        //评论微博
        function CommentWeibo(weiboId) {
            var commentTxt = $("#commentTxt_" + weiboId).val();
            //ajax post
            $.post("AjaxProcessPage.aspx?t=" + new Date(), { type: "CommentWeibo", msg: commentTxt, weiboId: weiboId }, function (data) {
                if (data.toString() == "OK") {
                    alert('回复成功！');
                    InitComment(weiboId);
                    InitComment(weiboId);
                    $(".Commit").css("float", ""); //IE CSS bug
                    $(".Commit").css("float", "right"); //IE CSS bug
                }
                else {
                    alert('回复失败');
                }
            });
        }

        function DeleteWeiboByVip(weiboId) {
            if (confirm('确认删除？')) {
                $.post("AjaxProcessPage.aspx?t=" + new Date(), { type: "DeleteWeiboByVip", weiboId: weiboId }, function (data) {
                    if (data.toString() == "OK") {
                        window.location = window.location;
                    }
                    else {
                        alert('删除失败');
                    }
                });
            }
        }

        function ShowBigPic(url) {
            window.open(url, 'newwindow');
        }
        $(function () {
            $("#CheckBox1").click(function () {
                $("#pubFile").slideToggle();
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="titleBar">
        <ul class="titleBarUl">
            <li class="titleBarLiItem" id="title_logo"></li>
            <li class="titleBarLiItem"><a href="home.aspx">首 页</a></li>
            <li class="titleBarLiItem"><a href="Me.aspx?uid=<%=UserId %>">
                <%=NickName %></a></li>
            <li class="titleBarLiItem" id="title_search">
                <div style="width: 200px;">
                    <input class="input W_no_outline" type="text" maxlength="40" node-type="searchInput"
                        id="txtSearch" runat="server" value="搜索微博，找人" title="搜索文本框" tabindex="4" onfocus="showTxt(this.value);"
                        onblur="showTxt(this.value);">
                    <asp:Button ID="Button2" runat="server" Text="" CssClass="btnSearch" OnClick="Button2_Click" />
                </div>
            </li>
            <li class="titleBarLiItem"><a href="SelfInfo.aspx">账户信息</a></li>
            <li class="titleBarLiItem"><a href="UpdatePassword.aspx">修改密码</a></li>
            <li class="titleBarLiItem">
                <asp:LinkButton ID="LinkButton_exit" runat="server" OnClick="LinkButton_exit_Click"
                    OnClientClick="return confirm('确认退出？')">退出</asp:LinkButton>
            </li>
        </ul>
    </div>
    <div id="MessageBox">
        <%-- <ul>
            <li>1条新评论， <a href="http://weibo.com/comment/inbox?f=1&amp;topnav=1&amp;wvr=4" bpfilter="main"
                action-type="bp-link" target="_top">查看评论</a></li>
        </ul>--%>
    </div>
    <div class="BackWall">
        <div class="left">
            <div class="faceKuang">
                <div class="facePic">
                    <img src="" style="width: 100px; height: 100px;" runat="server" id="userFace" onclick="window.location='UpdateFace.aspx'"
                        alt="修改头像" />
                </div>
                <div class="count">
                    <ul class="outNav">
                        <li class="outNavLi">
                            <ul class="inNav">
                                <li><a href="FriendManage.aspx">
                                    <%=FriendListCount %></a></li>
                                <li><a href="FriendManage.aspx">关注</a></li>
                            </ul>
                        </li>
                        <li class="outNavLi">
                            <ul class="inNav">
                            </ul>
                        </li>
                        <li class="outNavLi">
                            <ul class="inNav">
                                <li><a href="Me.aspx?uid=<%=UserId %>">
                                    <%=PublishedCount %></a></li>
                                <li><a href="Me.aspx?uid=<%=UserId %>">微博</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <uc1:MessageList ID="MessageList1" runat="server" />
        </div>
        <div class="center">
            <div class="topInfo">
                <img src="../Images/send_weibo.png" style="margin-top: 6px;" />
            </div>
            <div id="pubNew">
                <div class="input">
                    <asp:TextBox ID="txtMessage" runat="server" Style="height: 55px;" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div id="pubFile" style="height: 30px; width: 500px; display: none">
                    <asp:FileUpload ID="FileUpload1" runat="server" />&nbsp;<span style="color: #f00">请上传jpg,gif,png,rar格式的文件，小于2MB
                    </span>
                </div>
                <div class="pubbtn">
                    <asp:CheckBox ID="CheckBox1" runat="server" />
                    <span style="color: #f77b12">上传文件</span>
                    <asp:DropDownList ID="ddl_theme" runat="server">
                        <asp:ListItem Text="选择主题" Value="0" Selected="True"> </asp:ListItem>
                        <asp:ListItem Text="开发" Value="1"> </asp:ListItem>
                        <asp:ListItem Text="测试" Value="2"> </asp:ListItem>
                        <asp:ListItem Text="客服" Value="3"> </asp:ListItem>
                        <asp:ListItem Text="市场" Value="4"> </asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="Btn_Publish" runat="server" Text="" Height="30px" Width="70px" CssClass="btnPub"
                        OnClick="Btn_Publish_Click" />
                </div>
            </div>
            <div id="MainContent">
                <asp:Repeater ID="Repeater_AllWeibo" runat="server">
                    <ItemTemplate>
                        <div class="contentItemKuang">
                            <div class="leftFace">
                                <img src="<%#((Model.WeiBo)Container.DataItem).Owner.Face %>" width="50px" height="50px" />
                            </div>
                            <div class="RightFont">
                                <%#((Model.WeiBo)Container.DataItem).Owner.Status==1?"<span class='vip'><a target='_blank' href='FriendIndex.aspx?uid="+((Model.WeiBo)Container.DataItem).Owner._id+"'>"+((Model.WeiBo)Container.DataItem).Writer+"</a></span>":"<a target='_blank' href='FriendIndex.aspx?uid="+((Model.WeiBo)Container.DataItem).Owner._id+"'>"+((Model.WeiBo)Container.DataItem).Writer+"</a>" %>：
                                <%#((Model.WeiBo)Container.DataItem).Owner.Status==1?"<span class='vipMessage'>"+((Model.WeiBo)Container.DataItem).Message+"</span>":((Model.WeiBo)Container.DataItem).Message %>
                                <%--图片，附件显示--%>
                                <p style="display: <%#((Model.WeiBo)Container.DataItem).File.FileType=="pic"?"block":"none" %>">
                                    <img src="<%#SetPicSize(((Model.WeiBo)Container.DataItem).File.FileURL) %>" onclick="ShowBigPic('<%#((Model.WeiBo)Container.DataItem).File.FileURL %>')" />
                                </p>
                                <p style="display: <%#((Model.WeiBo)Container.DataItem).File.FileType=="file"?"block":"none" %>">
                                    上传文件：<span style="color: #f77b12"><%#((Model.WeiBo)Container.DataItem).File.FileName%>
                                    </span>&nbsp; <a href="<%#((Model.WeiBo)Container.DataItem).File.FileURL%>">点此下载
                                    </a>
                                </p>
                                <p class="content_footer">
                                    <%#((Model.WeiBo)Container.DataItem).PublishDate.ToString("MM月dd日 HH:mm")%>
                                    <span class="itemAction"><%--转发 |--%> <a onclick="InitComment('<%#((Model.WeiBo)Container.DataItem)._id %>')">
                                        评论</a><span class='<%=user.Status==1?"showSpan":"hideSpan" %>'>| <a onclick="DeleteWeiboByVip('<%#((Model.WeiBo)Container.DataItem)._id %>')">
                                            删除</a></span></span>
                                </p>
                            </div>
                            <div class="Commit" id="commit_<%#((Model.WeiBo)Container.DataItem)._id%>">
                                <div class="ReplyText" id="replyTxt_<%#((Model.WeiBo)Container.DataItem)._id%>" style="display: none;">
                                    <textarea id="commentTxt_<%#((Model.WeiBo)Container.DataItem)._id%>" name="" rows=""
                                        cols="" style="margin: 0px 0px 3px; padding: 4px 4px 0px; border-color: rgb(198, 198, 198);
                                        border-style: solid; border-width: 0.916667px; font-size: 12px; font-family: Tahoma,宋体;
                                        word-wrap: break-word; line-height: 18px; overflow: hidden; height: 20px; width: 350px;"></textarea>
                                    <p class="pubReply">
                                        <input value="" type="button" class="btnComment" onclick="CommentWeibo('<%#((Model.WeiBo)Container.DataItem)._id%>')" />
                                    </p>
                                </div>
                                <%--回复微博的容器div--%>
                                <div id="CommitedContainer_<%#((Model.WeiBo)Container.DataItem)._id%>">
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <table id="Page2" runat="server" cellpadding="0" cellspacing="0" border="0" style="margin: 0 auto;
                    width: 484px;">
                    <tr>
                        <td style="text-align: center; font-size: 14px; height: 40px; line-height: 40px;">第&gt;
                            <font color="red" style="font-weight: bold">
                                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label></font>&lt;页&nbsp;
                            <asp:Label ID="Label3" runat="server" Text=""></asp:Label>&nbsp;
                            <asp:HyperLink ID="HyperLink1" runat="server"><span class="kuang">上一页</span></asp:HyperLink>&nbsp;<asp:HyperLink
                                ID="HyperLink2" runat="server"><span  class="kuang">下一页</span></asp:HyperLink>
                            &nbsp;&nbsp;&nbsp; 共&gt; <font color="red" style="font-weight: bold">
                                        <asp:Label ID="Label2" runat="server" Text=""></asp:Label></font>&lt;页
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="right">
            <div class="rDiv">
                <p style="font-size: 14px; color: #ff006e; font-weight: bold;  padding-left:30px;
                    margin-bottom: 20px; margin-top: 10px; border-bottom: dotted 1px #1A5639;padding-bottom:10px;">
                    <img src="../Images/tip.png" style="width:30px;height: 30px;" /> 企业精神
                </p>
                <p class="pitem">效益源自创新</p>
                <p class="pitem">超越自我、追求卓越</p>
                <p class="pitem">以人才为根本，以市场为导向</p>
                <p class="pitem">以质量为保证，以服务为宗旨</p>
                <p class="pitem">超全心全意传递祝福</p>
                <p class="pitem">尽职尽责开拓创新</p>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
