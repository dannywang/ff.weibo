﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using Mongo;

namespace Web.Customer
{
    public partial class MyComment : System.Web.UI.Page
    {
        public User currentUser;
        protected void Page_Load(object sender, EventArgs e)
        {
            currentUser = (User)Session["CurrentUser"];
            GetData();
            TopBar1.UserId = currentUser._id.ToString();
            TopBar1.NickName = currentUser.NickName;
        }

        public void GetData()
        {
            List<Comment> list=DBTools.GetMyReceivedComment(currentUser.LoginName);
            Repeater1.DataSource = list;
            Repeater1.DataBind();
        }

        public Comment GetSourceInfo(string targetId)
        {
           return DBTools.GetCommentByTargetId(targetId);
        }
    }
}