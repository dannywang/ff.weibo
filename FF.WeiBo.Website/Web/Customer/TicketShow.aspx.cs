﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using Mongo;

namespace Web.Customer
{
    public partial class TicketShow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindData();
        }

        public void BindData()
        {
            if (Request.QueryString["tid"] != null)
            {
                string tid = Request.QueryString["tid"].ToString();
                Ticket tic = new Ticket();
                tic = DBTools.GetTicketByTid(tid);
                lblTitle.Text = tic.title;
                string listStr = "";
                for (int i = 0; i < tic.items.Length; i++)
                {
                    listStr += "<div class=\"add\" >" + "<input name=\"ticket\" id=\"Radio" + (i + 1) + "\" type=\"radio\" value='" + tic.items[i].ToString() + "'/>" + (i + 1) + "." + tic.items[i].ToString() + "</div>";
                }
                lblList.Text = listStr;
            }
        }
    }
}