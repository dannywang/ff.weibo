﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateTicket.aspx.cs" Inherits="Web.Customer.CreateTicket" %>

<%@ Register Src="TopBar.ascx" TagName="TopBar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../CSS/type.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/customer.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1-vsdoc.js"></script>
    <script type="text/javascript" src="../Scripts/Common.js"></script>
    <style>
        ul {
            margin: 0 auto;
            width: 520px;
        }

        .item {
            height: 40px;
            width: 400px;
            float: left;
        }

        #list {
            height: auto;
        }

        .iteml {
            height: 40px;
            width: 100px;
            float: left;
            font-size: 12px;
            font-weight: bold;
            color: #808080;
            text-align: right;
        }

        .txt {
            width: 300px;
        }

        .txtl {
            width: 290px;
        }

        .add {
            height: 35px;
            line-height: 35px;
        }
    </style>
    <script>
        var n = 3;
        $(function () {
            $("#btnAddItem").click(function () {
                var div = "<div class=\"add\" id=\"d" + n + "\">" + n + ".<input type=\"text\" id=\"item" + n + "\" class=\"txtl\" /></div>";
                $("#list").append(div);
                n++;
            });

            $("#btnPub").click(function () {
                var title = $("#title").val();
                //var weiboMessage = $("#weibo").val();
                var items = new Array();
                for (var i = 1; i < 10; i++) {
                    if ($("#item" + i).val() != undefined) {
                        items.push($("#item" + i).val());
                    }
                }
                var newTicket = {
                    "title": title,
                    //"weiboMessage": weiboMessage,
                    "items": items,
                    "station": 0
                };
                $.post("AjaxProcessPage.aspx?t=" + new Date(), { type: "CreateTicket", newTicket: CommonJS.ToSerialize(newTicket) }, function (data) {
                    if (data == "OK") {
                        alert("投票发布成功！");
                        window.location = 'Home.aspx';
                    }
                });

            });

        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <uc1:TopBar ID="TopBar1" runat="server" />
    <div class="ManageBack">
        <p class="fTitle">发起新投票</p>
        <ul>
            <li class="iteml">投票标题：</li>
            <li class="item">
                <input type="text" id="title" class="txt" /></li>
        </ul>
        <ul>
            <li class="iteml">投票选项：</li>
            <li class="item" id="list">
                <div class="add" id="d1">
                    1.<input type="text" id="item1" class="txtl" />
                </div>
                <div class="add" id="d2">
                    2.<input type="text" id="item2" class="txtl" />
                </div>
            </li>
        </ul>
        <ul>
            <li class="iteml"></li>
            <li class="item"><a href="#" id="btnAddItem">添加选项</a>
        </ul>
        <%-- <ul>
            <li class="iteml">微博文字：</li>
            <li class="item">
                <textarea class="txt" id="weibo"></textarea></li>
        </ul>--%>
        <ul style="margin: 0 auto; width: 300px;">
            <li style="margin-top: 100px; text-align: center;">
                <input type="button" value="发起投票" id="btnPub" class="btn" />
            </li>
        </ul>
    </div>
    </form>
</body>
</html>
