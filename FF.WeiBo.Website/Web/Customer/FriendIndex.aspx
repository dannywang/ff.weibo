﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FriendIndex.aspx.cs" Inherits="Web.Customer.FriendIndex" %>

<%@ Register Src="TopBar.ascx" TagName="TopBar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>我的主页</title>
    <link href="../CSS/type.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/customer.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Button1").click(function () {
                $("#msgBox").toggle();
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <uc1:TopBar ID="TopBar1" runat="server" />
    <div class="BackWall">
        <div class="left2">
            <div class="faceKuang2">
                <div class="facePic2">
                    <img runat="server" id="userFace" src="" style="width: 150px; height: 150px;" onclick="window.location='UpdateFace.aspx'"
                        alt="修改头像" />
                </div>
                <div class="count">
                    <ul class="outNav">
                        <li class="outNavLi">
                            <ul class="inNav">
                                <li>
                                    <%=FriendListCount %></li>
                                <li>关注</li>
                            </ul>
                        </li>
                        <li class="outNavLi"></li>
                        <li class="outNavLi">
                            <ul class="inNav">
                                <li>
                                    <%=PublishedCount %></li>
                                <li>微博</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="userMessageBox" style="float: left; padding-left: 50px;">
                <table cellpadding="0" cellspacing="0" border="0" style="/* background-color: #62B28C;
                    */ width: 150px; margin-left: 30px; margin-top: 40px;">
                    <tr height="32px">
                        <td class="messageImg">
                            <div id="homeI">
                            </div>
                        </td>
                        <td class="messageFont"><a href="home.aspx">首 页</a></td>
                    </tr>
                    <tr height="32px">
                        <td class="messageImg">
                            <div id="atI">
                            </div>
                        </td>
                        <td class="messageFont">投 票</td>
                    </tr>
                    <tr height="32px">
                        <td class="messageImg">
                            <div id="ComI">
                            </div>
                        </td>
                        <td class="messageFont"><a href="MyComment.aspx">评 论</a></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="center">
            <div class="topNick">
                <font>
                    <asp:Label ID="lblNickName" runat="server" Text="" Font-Size="20px" Font-Bold="true"></asp:Label>
                </font>
                <div style="margin-top: 14px; margin-left: 50px; padding-bottom: 10px;">
                    <asp:Label ID="lblDesp" runat="server" Text="" Font-Size="16px" ForeColor="#666"></asp:Label>
                </div>
            </div>
            <div class="weiboType">
                <input id="Button1" type="button" value="发私信" class="btn" />
            </div>
            <div class="SendMessage" id="msgBox">
                <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Height="70px" Width="400px"></asp:TextBox><br />
                <asp:Button ID="btnPubMesage" runat="server" Text="" Height="30px" CssClass="btnPub"
                    Width="71px" OnClick="btnPubMesage_Click" />
            </div>
            <div id="MainContent">
                <asp:Repeater ID="Repeater_PubData" runat="server">
                    <ItemTemplate>
                        <div class="contentItemKuang">
                            <div class="leftFace">
                                <img src='<%#((Model.WeiBo)Container.DataItem).Owner.Face==null?"Face/default.gif":((Model.WeiBo)Container.DataItem).Owner.Face%>'
                                    width="50px" height="50px" />
                            </div>
                            <div class="RightFont">
                                <%#((Model.WeiBo)Container.DataItem).Message %>
                                <p class="content_footer">
                                    <%#((Model.WeiBo)Container.DataItem).PublishDate.ToString() %>
                                    <span class="itemAction">评论&nbsp;| &nbsp; </span>
                                </p>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div class="right">
        </div>
    </div>
    </form>
</body>
</html>
