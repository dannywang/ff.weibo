﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdatePassword.aspx.cs"
    Inherits="Web.Customer.UpdatePassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../CSS/login.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <h1 style="margin-bottom: 20px;">修改密码</h1>
    <p class="Error">
        <asp:Label ID="message" runat="server" Text=""></asp:Label>
    </p>
    <p>
        <label for="old">
            原密码：
        </label>
        <input type="password" id="old" runat="server" style="width: 200px;">
    </p>
    <p>
        <label for="new1">
            新密码：
        </label>
        <input type="password" id="new1" runat="server" style="width: 200px;">
    </p>
    <p>
        <label for="new2">
            确认密码：
        </label>
        <input type="password" id="new2" runat="server" style="width: 200px;">
    </p>
    <p>
        <asp:LinkButton ID="btnUpdate" runat="server" CssClass="submit" OnClick="btnUpdate_Click">更新密码</asp:LinkButton>
        <asp:LinkButton ID="btnReturn" runat="server" CssClass="submit2" OnClick="btnReturn_Click">返回首页</asp:LinkButton>
    </p>
    </form>
</body>
</html>
