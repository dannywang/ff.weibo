﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using Mongo;

namespace Web.Customer
{
    public partial class FriendManage : System.Web.UI.Page
    {
        public User user = new User();
        protected void Page_Load(object sender, EventArgs e)
        {
            user = (User)Session["CurrentUser"];
            GetData();
            TopBar1.UserId = user._id.ToString();
            TopBar1.NickName = user.NickName;
        }

        public void GetData()
        {
            List<User> list = DBTools.GetUserFriendListByUId(user._id.ToString());
            Repeater1.DataSource = list;
            Repeater1.DataBind();
        }

        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                string FriendUserId = e.CommandArgument.ToString();
                DBTools.RemoveFriend(FriendUserId);
                GetData();
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "", "<script>alert('好友取消成功！');</script>");
            }
        }
    }


}