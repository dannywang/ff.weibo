﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TicketShow.aspx.cs" Inherits="Web.Customer.TicketShow" %>

<%@ Register Src="TopBar.ascx" TagName="TopBar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../CSS/type.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/customer.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1-vsdoc.js"></script>
    <script type="text/javascript" src="../Scripts/Common.js"></script>
    <style>
        ul {
            margin: 0 auto;
            width: 520px;
        }

        .item {
            height: 40px;
            width: 400px;
            float: left;
        }

        #list {
            height: auto;
        }

        .iteml {
            height: 40px;
            width: 100px;
            float: left;
            font-size: 12px;
            font-weight: bold;
            color: #808080;
            text-align: right;
        }

        .txt {
            width: 300px;
        }

        .txtl {
            width: 290px;
        }

        .add {
            height: 35px;
            line-height: 35px;
        }
    </style>
    <script>
        function GetRequest() {
            var url = location.search; //获取url中"?"符后的字串
            var theRequest = new Object();
            if (url.indexOf("?") != -1) {
                var str = url.substr(1);
                strs = str.split("&");
                for (var i = 0; i < strs.length; i++) {
                    theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
                }
            }
            return theRequest;
        }

        $(function () {
            $("#btnPub").click(function () {
                var selected = $("input[name='ticket']:checked").val();
                var Request = new Object();
                Request = GetRequest();
                
                tid = Request['tid'];

                $.post("AjaxProcessPage.aspx?t=" + new Date(), { type: "AddTicket", value: selected, tid: tid }, function (data) {
                    if (data.toString() == "OK") {
                        GetTicketResult(tid);
                        $("#btnPub").attr("disabled", "disabled");
                        $("#btnPub").css("backgroundColor", "#dedede");
                    }
                    else {

                    }
                });
            });
        });

        function GetTicketResult(tid)
        {
            $.post("AjaxProcessPage.aspx?t=" + new Date(), { type: "GetTicketResult", tid: tid }, function (data) {
                if (data.length>0) {
                    var jsonResult = eval("(" + data + ")");
                    var items = $("input[name='ticket']");
                    for (var i = 0; i < items.length; i++) {
                        for (var j = 0; j < jsonResult.length; j++) {
                            if (jsonResult[j].name == items[i].value)
                            {
                                $(items[i]).before("<span style='color:red'>[投票数：" + jsonResult[j].value + "]</span>");
                            }
                        }
                    }
                    
                }
                else {

                }
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <uc1:TopBar ID="TopBar1" runat="server" />
    <div class="ManageBack">
        <p class="fTitle">查看投票</p>
        <ul>
            <li class="iteml">投票标题：</li>
            <li class="item">
                <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
            </li>
        </ul>
        <ul>
            <li class="iteml">投票选项：</li>
            <li class="item" id="list">
                <asp:Label ID="lblList" runat="server" Text=""></asp:Label>
            </li>
        </ul>
        <ul>
            <li class="iteml"></li>
            <li class="item">
        </ul>
        <ul style="margin: 0 auto; width: 300px;">
            <li style="margin-top: 100px; text-align: center;">
                <input type="button" value="投 票" id="btnPub" class="btn" style="height: 30px; width: 100px;
                    font-weight: bold;" />
            </li>
        </ul>
    </div>
    </form>
    <script>
        
        var Request = new Object();
        Request = GetRequest();
        tid = Request['tid'];
        $.post("AjaxProcessPage.aspx?t=" + new Date(), { type: "CheckTicket", tid: tid }, function (data) {
            if (data.toString() == "OK") {
                GetTicketResult(tid);
                $("#btnPub").attr("disabled", "disabled");
                $("#btnPub").css("backgroundColor", "#dedede");
            }
        });
    </script>
</body>
</html>
