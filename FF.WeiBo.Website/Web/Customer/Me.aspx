﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Me.aspx.cs" Inherits="Web.Customer.Me" %>

<%@ Register Src="MessageList.ascx" TagName="MessageList" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>我的主页</title>
    <link href="../CSS/type.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/customer.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnShowMessage").click(function () {
                $("#MyMessage").slideToggle();
            });

            var Request = new Object();
            Request = GetRequest();
            var shortmsg;
            shortmsg = Request['shortmsg'];

            if (shortmsg == "true") {
                $("#MyMessage").slideToggle();
            }
        });

        function fReMessage(uid, nickName) {
            $("#txtReMessage").val("回复 " + nickName + ":");
            $("#hdUid").val(uid);
        }

        function GetRequest() {
            var url = location.search; //获取url中"?"符后的字串
            var theRequest = new Object();
            if (url.indexOf("?") != -1) {
                var str = url.substr(1);
                strs = str.split("&");
                for (var i = 0; i < strs.length; i++) {
                    theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
                }
            }
            return theRequest;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="titleBar">
        <ul class="titleBarUl">
            <li class="titleBarLiItem" id="title_logo"></li>
            <li class="titleBarLiItem"><a href="home.aspx">首 页</a></li>
            <li class="titleBarLiItem"><a href="Me.aspx?uid=<%=puid%>">
                <%=NickName%></a></li>
            <li class="titleBarLiItem"><a href="SelfInfo.aspx">账户信息</a></li>
            <li class="titleBarLiItem"><a href="UpdatePassword.aspx">修改密码</a></li>
            <li class="titleBarLiItem">
                <asp:LinkButton ID="LinkButton_exit" runat="server" OnClick="LinkButton_exit_Click"
                    OnClientClick="return confirm('确认退出？')">退出</asp:LinkButton>
            </li>
        </ul>
    </div>
    <div class="BackWall">
        <div class="left2">
            <div class="faceKuang2">
                <div class="facePic2">
                    <img runat="server" id="userFace" src="" style="width: 150px; height: 150px;" onclick="window.location='UpdateFace.aspx'"
                        alt="修改头像" />
                </div>
                <div class="count">
                    <ul class="outNav">
                        <li class="outNavLi">
                            <ul class="inNav">
                                <li><a href="FriendManage.aspx">
                                    <%=FriendListCount %></a></li>
                                <li><a href="FriendManage.aspx">关注</a></li>
                            </ul>
                        </li>
                        <li class="outNavLi">
                            <ul class="inNav">
                                <%-- <li>222</li>
                                <li>粉丝</li>--%>
                            </ul>
                        </li>
                        <li class="outNavLi">
                            <ul class="inNav">
                                <li>
                                    <%=PublishedCount %></li>
                                <li>微博</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="userMessageBox" style="float: left; padding-left: 50px;">
                <table cellpadding="0" cellspacing="0" border="0" style="/* background-color: #62B28C;
                    */ width: 150px; margin-left: 30px; margin-top: 40px;">
                    <tr height="32px">
                        <td class="messageImg">
                            <div id="homeI">
                            </div>
                        </td>
                        <td class="messageFont"><a href="home.aspx">首 页</a></td>
                    </tr>
                    <tr height="32px">
                        <td class="messageImg">
                            <div id="atI">
                            </div>
                        </td>
                        <td class="messageFont"><a href="CreateTicket.aspx">投 票</a></td>
                    </tr>
                    <tr height="32px">
                        <td class="messageImg">
                            <div id="ComI">
                            </div>
                        </td>
                        <td class="messageFont"><a href="MyComment.aspx">评 论</a></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="center">
            <div class="topNick">
                <font>
                    <asp:Label ID="lblNickName" runat="server" Text="" Font-Size="20px" Font-Bold="true" ForeColor="#3366ff"></asp:Label>
                </font>
                <div style="margin-top: 14px; margin-left: 50px; padding-bottom: 10px;">
                    <asp:Label ID="lblDesp" runat="server" Text="" Font-Size="16px" ForeColor="#666"></asp:Label>
                </div>
            </div>
            <div class="weiboType">
                选择主题：
                <asp:DropDownList ID="ddl_theme" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_theme_SelectedIndexChanged">
                    <asp:ListItem Text="全部主题" Value="0" Selected="True"> </asp:ListItem>
                    <asp:ListItem Text="开发" Value="1"> </asp:ListItem>
                    <asp:ListItem Text="测试" Value="2"> </asp:ListItem>
                    <asp:ListItem Text="客服" Value="3"> </asp:ListItem>
                    <asp:ListItem Text="市场" Value="4"> </asp:ListItem>
                </asp:DropDownList>&nbsp;&nbsp;&nbsp;
                <input id="btnShowMessage" type="button" value="我的私信" class="btn" />
            </div>
            <%--我的私信部分--%>
            <div class="MyMessage" id="MyMessage" runat="server">
                <p style="font-size: 14px; color: #808080; margin-bottom: 10px;">我的私信列表</p>
                <div>
                    <asp:TextBox ID="txtReMessage" runat="server" TextMode="MultiLine" Width="369px"
                        Height="40px"></asp:TextBox>
                    <input id="hdUid" type="hidden" runat="server" />
                    <asp:Button ID="btnRe" runat="server" Text="回复" OnClick="btnRe_Click" CssClass="btn" />
                </div>
                <div>
                    <asp:Repeater ID="Repeater_ReMessage" runat="server">
                        <ItemTemplate>
                            <ul style="border-bottom-style: dotted; border-bottom: dotted 1px #dedede;">
                                <li class="msg_nick">
                                    <%#((Model.ShortMessage)Container.DataItem).PublishUser.NickName %>：</li>
                                <li class="msg_content">
                                    <%#((Model.ShortMessage)Container.DataItem).Message %></li>
                                <li class="msg_re"><a onclick="fReMessage('<%#((Model.ShortMessage)Container.DataItem).PublishUser.LoginName %>','<%#((Model.ShortMessage)Container.DataItem).PublishUser.NickName %>')">
                                    回复</a></li>
                            </ul>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <%--我的私信部分end--%>
            <div id="MainContent">
                <asp:Repeater ID="Repeater_PubData" runat="server" OnItemCommand="Repeater_PubData_ItemCommand">
                    <ItemTemplate>
                        <div class="contentItemKuang">
                            <div class="leftFace">
                                <img src='<%#((Model.WeiBo)Container.DataItem).Owner.Face==null?"Face/default.gif":((Model.WeiBo)Container.DataItem).Owner.Face%>'
                                    width="50px" height="50px" />
                            </div>
                            <div class="RightFont">
                                <%#((Model.WeiBo)Container.DataItem).Message %>
                                <p class="content_footer">
                                    <%#((Model.WeiBo)Container.DataItem).PublishDate.ToString() %>
                                    <span class="itemAction"><%--评论&nbsp;|--%> &nbsp;
                                        <asp:LinkButton ID="LinkButton1" OnClientClick="return confirm('确认删除？')" runat="server"
                                            CommandName="delete" CommandArgument="<%#((Model.WeiBo)Container.DataItem)._id %>">删除</asp:LinkButton>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <p id="NewUserShow" runat="server" visible="false" style="text-align: center; font-size: 14px;
                    font-weight: bold; color: #ff6a00; margin-top: 150px;">
                    您还没有发布微博，快去发布您的第一篇微博吧！ <a href="home.aspx">点此发布</a>
                </p>
            </div>
        </div>
        <div class="right">
        </div>
    </div>
    </form>
</body>
</html>
