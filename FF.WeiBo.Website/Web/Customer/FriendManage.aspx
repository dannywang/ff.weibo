﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FriendManage.aspx.cs" Inherits="Web.Customer.FriendManage" %>

<%@ Register Src="TopBar.ascx" TagName="TopBar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../CSS/type.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/customer.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc1:TopBar ID="TopBar1" runat="server" />
    <div class="ManageBack">
        <p class="fTitle">我关注的同事</p>
        <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand">
            <ItemTemplate>
                <div class="searchItem">
                    <div style="width: 50px; height: 50px; float: left;">
                        <img src="<%#((Model.User)Container.DataItem).Face %>" style="width: 50px; height: 50px;" />
                    </div>
                    <div style="width: 300px; height: 50px; float: left; margin-left: 20px;">
                        <p style="margin: 6px; font-size: 14px; font-weight: bold; padding-left: 10px;">
                            <a href="FriendIndex.aspx?uid=<%#((Model.User)Container.DataItem)._id %>" target="_blank"><%#((Model.User)Container.DataItem).NickName %></a>
                        </p>
                        <p style="margin-left: 30px; color: #666; margin-top: 4px;">
                            <%#((Model.User)Container.DataItem).Description %>
                        </p>
                    </div>
                    <div style="width: 70px; height: 50px; float: left; margin-left: 10px;">
                        <p style="margin-top: 20px;">
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="delete" CommandArgument="<%#((Model.User)Container.DataItem)._id%>"
                                OnClientClick="return confirm('确认取消关注？')">取消关注</asp:LinkButton>
                        </p>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    </form>
</body>
</html>
