﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateFace.aspx.cs" Inherits="Web.Customer.UpdateFace" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../CSS/login.css" rel="stylesheet" type="text/css" />
   
</head>
<body>
    <form id="form1" runat="server">
    <h1 style="margin-bottom: 20px;">
        修改头像</h1>
    <p class="Error">
        <asp:Label ID="message" runat="server" Text=""></asp:Label>
    </p>
    <p>
        <label for="nickName">
            上传头像：</label>
        <asp:FileUpload ID="FileUpload1" runat="server" Width="260px" />
    </p>
    <p>只允许上传jpg, png, gif, bmp格式的文件，大小不超过1mb</p>
    <p>
        <asp:LinkButton ID="btnUpdate" runat="server" CssClass="submit" OnClick="btnUpdate_Click">更新头像</asp:LinkButton>
        <asp:LinkButton ID="btnReturn" runat="server" CssClass="submit2" OnClick="btnReturn_Click">返回首页</asp:LinkButton>
    </p>
    </form>
</body>
</html>
