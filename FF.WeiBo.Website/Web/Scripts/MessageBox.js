﻿/// <reference path="jquery-1.4.1.js" />
/// <reference path="jquery-1.4.1-vsdoc.js" />
$(function () {
    getData(); //首次立即加载
    window.setInterval(getData, 2000); //循环执行！！
})

function getData() {

    $.post("AjaxProcessPage.aspx?t=" + new Date(), { type: "GetNewMessage" }, function (data) {
        if (data.length > 0) {
            var jsonResult = eval("(" + data + ")");
            var count_comment = 0;
            var count_message = 0;
            var html = "";
            if (jsonResult.list_Comment.length > 0) {
                for (var i = 0; i < jsonResult.list_Comment.length; i++) {
                    count_comment++;
                }
            }
            if (jsonResult.list_ShortMessage.length > 0) {
                for (var i = 0; i < jsonResult.list_ShortMessage.length; i++) {
                    count_message++;
                }
            }
            html += "<ul>";
            if (count_comment > 0) {
                html += '<li>' + count_comment + '条新评论， <a onclick="ReadMessageByType(\'comment\')" href="MyComment.aspx" bpfilter="main" action-type="bp-link" target="_top">查看评论</a></li>';
            }
            if (count_message > 0) {
                html += '<li>' + count_message + '条新私信， <a onclick="ReadMessageByType(\'shortMessage\')" href="Me.aspx?shortmsg=true&uid=' + jsonResult.currentUserId + '" bpfilter="main" action-type="bp-link" target="_top">查看私信</a></li>';
            }



            html += "</ul>";

            $("#MessageBox").html(html);
            if (count_comment + count_message == 0) {
                $("#MessageBox").hide();
            }

        }
        else {
            //alert('回复失败');
        }

    });
}
function ReadMessageByType(type) {
    $.post("AjaxProcessPage.aspx?t=" + new Date(), { type: "ReadMessageByType", typeName: type }, function (data) {

    });
}