﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Web.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>登陆微博</title>
    <link href="CSS/login.css" rel="stylesheet" type="text/css" />
    <link rel="icon" href="/favicon.ico" type="image/x-icon" media="screen" />
    <script type="text/javascript" src="Scripts/jquery-1.4.1.js"></script>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" media="screen" />
    <style>
        .tt {
            position: absolute;
            top: 0;
            left: 0;
            z-index: -100;
        }
    </style>
    <script>
        $(function () {
            //定义回车事件
            if (document.addEventListener) {//如果是Firefox
                document.addEventListener("keypress", fireFoxHandler, true);
            }
            else {
                document.attachEvent("onkeypress", ieHandler);
            }

            function fireFoxHandler(evt) {

                if (evt.keyCode == 13) {

                    $("#btnLogin")[0].click();
                }
            }
            function ieHandler(evt) {

                if (evt.keyCode == 13) {
                    $("#btnLogin")[0].click();
                }
            }
        });


    </script>
</head>
<body>
    <div class="tt">
        <img src="Images/loginBG.jpg" onload="this.width=screen.width;this.height=screen.height;" />
    </div>
    <form id="form1" runat="server">
    <h1 style="margin-bottom: 20px;">
        <img src="Images/logos.png" style="height: 20px; width: 20px; display: inline;" />
        微博登陆<span style=" font-size: 12px; font-weight: normal; float: right;">欢迎访问企业微博</span></h1>
    <p class="Error">
        <asp:Label ID="message" runat="server" Text=""></asp:Label>
    </p>
    <p>
        <label for="loginName">
            登录邮箱：
        </label>
        <input type="text" id="loginName" runat="server" style="width: 200px;">
    </p>
    <p>
        <label for="password">
            密码：
        </label>
        <input type="password" id="password" runat="server" style="width: 200px;">
    </p>
    <p>
        <asp:LinkButton ID="btnLogin" runat="server" CssClass="submit" OnClick="btnLogin_Click">登录微博</asp:LinkButton>
    </p>
    </form>
</body>
</html>
