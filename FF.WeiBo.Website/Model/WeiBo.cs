﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;

namespace Model
{
    public class WeiBo
    {
        //需要注意的是主键Id属性的类型是ObjectId。在增删改查中，这个Id的作用是非常重要的。
        public ObjectId _id { set; get; }
        public User Owner { set; get; }
        public string Message { set; get; }
        public string WriterId { set; get; }
        public string Writer { set; get; }
        public string Theme { set; get; }
        public Files File { set; get; }
        public int Status { set; get; }
        public DateTime PublishDate { set; get; }
    }

    public class Files
    {
        //需要注意的是主键Id属性的类型是ObjectId。在增删改查中，这个Id的作用是非常重要的。
        public ObjectId _id { set; get; }
        public string FileName { set; get; }
        public string Size { set; get; }
        public string FileType { set; get; }
        public string FileURL { set; get; }
    }

    public class Comment
    {
        //需要注意的是主键Id属性的类型是ObjectId。在增删改查中，这个Id的作用是非常重要的。
        public ObjectId _id { set; get; }
        /// <summary>
        /// 评论者
        /// </summary>
        public User PublishUser { set; get; }
        public string PublishUserId { set; get; }
        public string CommentWeiboId { set; get; }
        public WeiBo CommentWeibo { set; get; }
        public string CommentMessage { set; get; }
        public DateTime AddDate { set; get; }
        public int Status { set; get; }
        public string TargetId { set; get; }
        
    }

    //public class NewMessageList
    //{
    //    //需要注意的是主键Id属性的类型是ObjectId。在增删改查中，这个Id的作用是非常重要的。
    //    public ObjectId _id { set; get; }
       
    
    //}

    public class ShortMessage
    {
        public ObjectId _id { set; get; }
        public User PublishUser { set; get; }
        public User ReceiveUser { set; get; }
        public string PublishUserLoginName { set; get; }
        public string ReceiveUserLoginName { set; get; }
        public string  Message { set; get; }
        public DateTime AddDate { set; get; }
        public int Status { set; get; }
    }
}
