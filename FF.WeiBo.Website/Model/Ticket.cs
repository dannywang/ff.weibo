﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
 

namespace Model
{
    public class Ticket
    {
        public ObjectId _id { set; get; }
        public string pubUserLoginName { set; get; }
        public User pubUser { set; get; }
        public string title { set; get; }
        public string[] items { set; get; }
        public int station { set; get; }
        public DateTime addDate { set; get; }
        public string tid { set; get; }
    }

    public class ChooseTicket
    {
        public ObjectId _id { set; get; }
        public string TargetTid { set; get; }
        public Ticket TargetTicket { set; get; }
        public User ChooseUser { set; get; }
        public string ChooseUserLoginName { set; get; }
        public string ChooseItem { set; get; }
    }

    public class TicketShow {
        public string name{ set; get; }
        public int value{ set; get; }
    }
}
