﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
 

namespace Model
{
    public class User
    {
        //需要注意的是主键Id属性的类型是ObjectId。在增删改查中，这个Id的作用是非常重要的。
        public ObjectId _id { set; get; }
        public string LoginName { set; get; }
        public string NickName { set; get; }
        public string Password { set; get; }
        public string Face { set; get; }
        public string Description { set; get; }
        public int Status { set; get; }
        public DateTime RegisterDate { set; get; }
    }

    public class Relation
    {
        public ObjectId _id { set; get; }
        public string UserId { set; get; }
        public string LoginName { set; get; }
        public string NickName { set; get; }
        public string RelationUserId { set; get; }
        public User RelationUser { set; get; }
        public int Station { set; get; }
        public DateTime AddDate { set; get; }

    }
}
